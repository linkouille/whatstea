// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "WhatsteaGameMode.generated.h"

UCLASS(minimalapi)
class AWhatsteaGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AWhatsteaGameMode();
};



