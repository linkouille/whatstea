#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Collectable.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UCollectable : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class WHATSTEA_API ICollectable
{
	GENERATED_BODY()

public:

	virtual void Gather(UObject* other){}


};
