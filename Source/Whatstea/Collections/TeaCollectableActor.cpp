#include "TeaCollectableActor.h"
#include "Whatstea/ToolBox.h"

#include "Whatstea/GameData.h"
#include "Whatstea/HUD/CollectableHUD.h"

// Sets default values
ATeaCollectableActor::ATeaCollectableActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ATeaCollectableActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATeaCollectableActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATeaCollectableActor::Gather(UObject* other)
{
	UGameData* gameData = Cast<UGameData>(other);
	if (gameData) {
		gameData->teaInventory[(int)type] += 1;

		if (gameData->collectableHUD) {
			gameData->collectableHUD->AddEvent(type);
		}

		Destroy();

	}


}

