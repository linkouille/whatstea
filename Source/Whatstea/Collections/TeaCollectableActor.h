#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "Whatstea/Collections/Collectable.h"
#include "Whatstea/GameData.h"

#include "TeaCollectableActor.generated.h"

UCLASS()
class WHATSTEA_API ATeaCollectableActor : public AActor, public ICollectable
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere)
		TEnumAsByte<TeaType> type; 

public:	
	// Sets default values for this actor's properties
	ATeaCollectableActor();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	

	void Gather(UObject* other);

};
