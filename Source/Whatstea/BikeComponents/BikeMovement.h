#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Whatstea/BikeComponents/Interfaces/Movement.h"

#include "BikeMovement.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class WHATSTEA_API UBikeMovement : public UActorComponent, public IMovement
{
	GENERATED_BODY()

public:
	//Properties:
	UPROPERTY(EditAnywhere)
		float gamepadTurnRate = 50;

	UPROPERTY(EditAnywhere)
		float maxStrenght = 100;

	// How much pedal strenght grow
	UPROPERTY(EditAnywhere)
		float strenghtGain = 0.5;

	UPROPERTY(EditAnywhere)
		float brakeStrenght = 2;

	// How much the slope acceleration curve affect bike
	UPROPERTY(EditAnywhere)
		float slopeAccelerationMult = 2.0f;

	UPROPERTY(EditAnywhere)
		float slopeDragMult = 0.5f;
	//Max strenght taht can be added to strenght
	UPROPERTY(EditAnywhere)
		float maxStrenghtGain = 20;

	// How much the drag curve affect bike
	UPROPERTY(EditAnywhere)
		float dragMult = 1;

	// How much the bike is slowing down without player interacting with it
	UPROPERTY(EditDefaultsOnly, Category = Curve)
		UCurveFloat* dragCurve;

	// How fast we go to the maximum acceleration potential
	UPROPERTY(EditAnywhere)
		float slopeAccelerationSpeed = 0.5f;

	// How much bike take speed in a decending slope (change over time in a decending slope)
	UPROPERTY(EditDefaultsOnly, Category = Curve)
		UCurveFloat* slopeAccelerationCurve;

	// How fast we go to the maximum drag potential
	UPROPERTY(EditAnywhere)
		float slopeDragSpeed = 0.5f;

	// How much bike slow down in a ascending slope (change over time in a rising slope)
	UPROPERTY(EditDefaultsOnly, Category = Curve)
		UCurveFloat* slopeDragCurve;

	UPROPERTY(EditDefaultsOnly, Category = Curve)
		UCurveFloat* brakeCurve;

	UPROPERTY(EditAnywhere)
		float brakeGainSpeed = 1.0f;

private:

	// current strenght accumulated in left pedal
	float cumulStrenght_L = 0;
	// Is left pedal pushed
	bool pushed_L = false;

	// current strenght accumulated in right pedal
	float cumulStrenght_R = 0;
	// Is right pedal pushed
	bool pushed_R = false;

	// current time in rising slope
	float slopeDragTime;
	// current time in decending slope
	float slopeAccelerationTime;

	float brakeTime = 0;
	bool isBraking;

	float pedalingTime;
	float pedalingBufferTime = 0.5f;


public:	
	// Sets default values for this component's properties
	UBikeMovement();

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void Setup(ABikeCharacter* p_character);

	UFUNCTION(BlueprintCallable)
	float GetSpeedRatio();

	/// <summary>
	/// Left pedal is pressed
	/// </summary>
	/// <param name="amount">How much we pressed the pedal (not used in strenth computing)</param>
	UFUNCTION()
		void PedalInput_L(const FInputActionValue& value);

	/// <summary>
	/// Right pedal is pressed
	/// </summary>
	/// <param name="amount">How much we pressed the pedal (not used in strenth computing)</param>
	UFUNCTION()
		void PedalInput_R(const FInputActionValue& value);

	/// <summary>
	/// Pressed Brake to stop the bike
	/// </summary>
	
	UFUNCTION()
		void BrakeInput();

	/// <summary>
	/// Flip flop function to stop or resume character movement
	/// </summary>
	UFUNCTION()
		void ChangeCanMoveInput();

	/// <summary>
	/// Turn the bike
	/// </summary>
	/// <param name="rate">How much we turn the bike, rate < 0 -> bike will go left & rate > 0 -> bike will go right</param>
	UFUNCTION()
		void TurnRateInput(const FInputActionValue& value);

	void BindKeys();

	UFUNCTION(BlueprintCallable)
	virtual bool IsPedaling();

	UFUNCTION(BlueprintCallable)
	virtual bool IsBraking();


protected:
	// Called when the game starts
	virtual void BeginPlay() override;

private:
	// Update Functions	
	/// <summary>
	/// Apply slope influence on the bike
	/// </summary>
	/// <param name="deltaTime"></param>
	void UpdateSlope(float deltaTime);

	void ApplyDrag(float deltaTime);

	/// <summary>
	/// Update cumulated strenght in the pedals and then apply current strenght to the character in the bike direction
	/// </summary>
	/// <param name="deltaTime"></param>
	void UpdateStrenghts(float deltaTime);	

	/// <summary>
	/// Callback function for when the character collide
	/// </summary>
	/// <param name="comp"></param>
	/// <param name="otherActor"></param>
	/// <param name="otherComp"></param>
	/// <param name="normalImpulse"></param>
	/// <param name="hit"></param>
	UFUNCTION()
		void OnCollisionEnter(UPrimitiveComponent* comp, AActor* otherActor, UPrimitiveComponent* otherComp, FVector normalImpulse, const FHitResult& hit);
};
