#pragma once

#include "CoreMinimal.h"
#include "Whatstea/BikeComponents/Interfaces/Movement.h"
#include "Components/ActorComponent.h"
#include "Whatstea/Inputs/InputHandler.h"
#include "OptionHolder.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class WHATSTEA_API UOptionHolder : public UActorComponent
{
	GENERATED_BODY()


private:

	TSubclassOf<class UOptionHUD> HUDClass;

	class UOptionHUD* HUD;

	UInputHandler* inputHandler;

	IMovement* movement;

	bool viewState = false;

public:	
	UOptionHolder();

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	
	void Setup(IMovement * p_movement);

	UFUNCTION()
		void ChangeHUDView();

	UFUNCTION()
		void ChangeCategory(const FInputActionValue& value);

	UFUNCTION()
		void Select();

	UFUNCTION()
		void Cancel();

	UFUNCTION()
		void ChangeOptions(const FInputActionValue& value);


protected:

	virtual void BeginPlay() override;


		
};
