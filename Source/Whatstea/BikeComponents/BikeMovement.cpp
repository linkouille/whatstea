#include "BikeMovement.h"

#include "Whatstea/BikeCharacter.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Whatstea/ToolBox.h"

UBikeMovement::UBikeMovement()
{
	PrimaryComponentTick.bCanEverTick = true;

	UE_LOG(LogTemp, Warning, TEXT("I just started running BikeMovement"));
}

void UBikeMovement::BeginPlay()
{
	Super::BeginPlay();
}

void UBikeMovement::Setup(ABikeCharacter* p_character)
{
	character = p_character;

	initialSpeed = character->GetCharacterMovement()->MaxWalkSpeed;

	GetOwner()->FindComponentByClass<UCapsuleComponent>()->OnComponentHit.AddDynamic(this, &UBikeMovement::OnCollisionEnter);

	BindKeys();
}

void UBikeMovement::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	//Physics
	if (canMove) {

		UpdateStrenghts(DeltaTime);

		UpdateSlope(DeltaTime);

		ApplyDrag(DeltaTime);

	}

	if(pedalingTime < pedalingBufferTime + 1)	pedalingTime += DeltaTime;

}

float UBikeMovement::GetSpeedRatio()
{
	return strenght / maxStrenght;
}


void UBikeMovement::TurnRateInput(const FInputActionValue& value)
{
	float rate = value.Get<float>();
	if (canMove)
		character->AddControllerYawInput(rate * gamepadTurnRate * GetWorld()->GetDeltaSeconds());
}

void UBikeMovement::PedalInput_L(const FInputActionValue& value)
{
	pushed_L = !pushed_L;

	if (!pushed_L) {
		strenght += cumulStrenght_L;
		cumulStrenght_L = 0;
	}
}

void UBikeMovement::PedalInput_R(const FInputActionValue& value)
{
	pushed_R = !pushed_R;

	if (!pushed_R) {
		strenght += cumulStrenght_R;
		cumulStrenght_R = 0;
	}
}


void UBikeMovement::BrakeInput()
{
	isBraking = !isBraking;
	if (!isBraking)
		brakeTime = 0;
}

void UBikeMovement::ChangeCanMoveInput()
{
	canMove = !canMove;
	strenght = 0;
}

void UBikeMovement::BindKeys()
{
	UInputHandler * inputHandler = GetOwner()->FindComponentByClass<UInputHandler>();
	if (!inputHandler)
		ToolBox::DEBUG_ONSCREEN_ERROR("ERROR: NO INPUTHANDLER ATTATCH");

	inputHandler->BindAction(UInputHandler::Brake, this, FName("BrakeInput"));

	inputHandler->BindAction(UInputHandler::Turn, this, FName("TurnRateInput"));

	inputHandler->BindAction(UInputHandler::Pedal_L, this, FName("PedalInput_L"));

	inputHandler->BindAction(UInputHandler::Pedal_R, this, FName("PedalInput_R"));

	inputHandler->BindAction(UInputHandler::Open_SB, this, FName("ChangeCanMoveInput"));

}

bool UBikeMovement::IsPedaling()
{

	if (pushed_R || pushed_L) {
		pedalingTime = 0;
	}
	return pedalingTime < pedalingBufferTime;
}

bool UBikeMovement::IsBraking()
{
	return isBraking;
}

void UBikeMovement::UpdateSlope(float deltaTime)
{
	if (!character) {
		ToolBox::DEBUG_ONSCREEN_ERROR("NO CHARACTER");
		return;
	}
	FHitResult hitOut;

	FVector start = character->GetActorLocation() + FVector::DownVector * character->GetCapsuleComponent()->GetScaledCapsuleHalfHeight();
	FVector end = start + character->GetActorUpVector() * -100;

	if (GetWorld()->LineTraceSingleByChannel(hitOut, start, end, ECC_Visibility)) {

		FVector slopeDirection = hitOut.ImpactNormal.Cross(hitOut.ImpactNormal.Cross(FVector::UpVector));

		float NdotF = FVector::DotProduct(hitOut.ImpactNormal, character->GetActorForwardVector());
		float sign = NdotF != 0 ? -NdotF / abs(NdotF) : 1;

		float projection = slopeDirection.Dot(character->GetActorForwardVector()) / character->GetActorForwardVector().Dot(character->GetActorForwardVector());

		isSloping = projection >= 0.0f;

		if (sign < 0) {
			strenght += slopeAccelerationMult * slopeAccelerationCurve->GetFloatValue(projection);


		}
		else if (sign > 0) {
			strenght += slopeDragMult * -slopeDragCurve->GetFloatValue(-projection);
		}
		
	}
}

void UBikeMovement::ApplyDrag(float deltatime)
{
	if (abs(strenght) > 0) {
		strenght += dragMult * dragCurve->GetFloatValue(abs(strenght / maxStrenght)) * -(strenght / abs(strenght));
	}

}

void UBikeMovement::UpdateStrenghts(float deltaTime)
{
	if (!character) {
		ToolBox::DEBUG_ONSCREEN_ERROR("NO CHARACTER");
		return;
	}
	//Udpate cumul strenght L and R	
	if (cumulStrenght_L < maxStrenghtGain && pushed_L)
		cumulStrenght_L += strenghtGain;

	if (cumulStrenght_R < maxStrenghtGain && pushed_R)
		cumulStrenght_R += strenghtGain;

	if (isBraking) {
		float brakeAmount = brakeStrenght * brakeCurve->GetFloatValue(brakeTime);

		if (strenght - brakeAmount >= 0) {
			strenght -= brakeAmount;
		}
		else {
			strenght = 0;
		}
		brakeTime += brakeGainSpeed * deltaTime;
	}

	// Apply Strenght Mult
	if (strenght / maxStrenght > 1)
		character->GetCharacterMovement()->MaxWalkSpeed = initialSpeed * strenght / maxStrenght;

	//limit force
	strenght = strenght > 200 ? 200 : strenght;

	// Apply Strenght
	const FRotator Rotation = character->Controller->GetControlRotation();
	const FRotator YawRotation(0, Rotation.Yaw, 0);
	const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
	character->AddMovementInput(Direction, strenght / maxStrenght);
}

void UBikeMovement::OnCollisionEnter(UPrimitiveComponent* comp, AActor* otherActor, UPrimitiveComponent* otherComp, FVector normalImpulse, const FHitResult& hit)
{
	strenght = 0;
}
