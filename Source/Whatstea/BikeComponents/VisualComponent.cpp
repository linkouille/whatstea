#include "VisualComponent.h"
#include "Whatstea/BikeComponents/BikeAnimationInstance.h"
#include "Components/SkeletalMeshComponent.h"
#include "Whatstea/BikeComponents/Interfaces/Movement.h"

#include "Whatstea/ToolBox.h"

UVisualComponent::UVisualComponent()
{
	PrimaryComponentTick.bCanEverTick = true;

}

void UVisualComponent::BeginPlay()
{
	Super::BeginPlay();
	
}

void UVisualComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	UpdateAnimationValue(DeltaTime);

	UpdateModelOrientation(DeltaTime);

	/*ToolBox::DEBUG_ONSCREEN_F(animationInst->speed, "Animation speed", DeltaTime);
	ToolBox::DEBUG_ONSCREEN_F(animationInst->left, "Animation left", DeltaTime);
	ToolBox::DEBUG_ONSCREEN_F(animationInst->right, "Animation right", DeltaTime);*/

	/*if (movement->IsPedaling()) ToolBox::DEBUG_ONSCREEN("Pedaling", DeltaTime);
	else ToolBox::DEBUG_ONSCREEN("Not Pedaling", DeltaTime);

	if (animationInst->starting) ToolBox::DEBUG_ONSCREEN("Starting", DeltaTime);
	else ToolBox::DEBUG_ONSCREEN("Not Starting", DeltaTime);*/
}

void UVisualComponent::Setup(IMovement* p_movement)
{
	USkeletalMeshComponent* skelmesh = (USkeletalMeshComponent *)GetOwner()->GetComponentsByTag(USkeletalMeshComponent::StaticClass(), "CharacterMesh")[0];
	if (!skelmesh) {
		ToolBox::DEBUG_ONSCREEN_ERROR("ERROR: NO SKEL MODEL ATTATCH");
		return;	
	}
	animationInst = Cast<UBikeAnimationInstance>(skelmesh->AnimScriptInstance);
	if (!animationInst) {
		ToolBox::DEBUG_ONSCREEN_ERROR("ERROR: NO ANIM INSTANCE ATTATCH");
		return;
	}
	movement = p_movement;

	inputHandler = GetOwner()->FindComponentByClass<UInputHandler>();
	if (!inputHandler) {
		ToolBox::DEBUG_ONSCREEN_ERROR("ERROR: NO BIKE CHARACTER ATTATCH");
		return;
	}
	inputHandler->BindAction(UInputHandler::Actions::Turn, this, "AnimationGetLeftRightValue");

	mesh = GetOwner()->FindComponentByClass<USkeletalMeshComponent>();
	if(!mesh) {
		ToolBox::DEBUG_ONSCREEN_ERROR("ERROR: NO CHARACTER MODEL ATTATCH");
		return;
	}
}

void UVisualComponent::UpdateAnimationValue(float deltaTime)
{

	animationInst->starting = movement->IsPedaling();
	animationInst->speed = movement->GetSpeedRatio();
	animationInst->sliding = movement->IsSloping();

}

void UVisualComponent::UpdateModelOrientation(float deltaTime)
{
	
	FHitResult hitOuts[2];

	FRotator r = mesh->GetRelativeRotation();

	FVector start[2] = { GetOwner()->GetActorLocation() + GetOwner()->GetActorRotation().RotateVector(wheelsPos[0]), 
		GetOwner()->GetActorLocation() + GetOwner()->GetActorRotation().RotateVector(wheelsPos[1]) };

	FVector end[2] = { start[0] + GetOwner()->GetActorUpVector() * -1000, start[1] + GetOwner()->GetActorUpVector() * -1000 };

	if (GetWorld()->LineTraceSingleByChannel(hitOuts[0], start[0], end[0], ECC_Visibility)
		&& GetWorld()->LineTraceSingleByChannel(hitOuts[1], start[1], end[1], ECC_Visibility)) {

		FVector slope = (hitOuts[0].ImpactPoint - hitOuts[1].ImpactPoint);
		slope.Normalize();

		FVector horizontalSlope = FVector::VectorPlaneProject(slope, FVector::UpVector);
		horizontalSlope.Normalize();

		float NdotF = FVector::DotProduct(hitOuts[0].ImpactNormal, GetOwner()->GetActorForwardVector());
		float sign = NdotF != 0 ? -NdotF / abs(NdotF) : 1;

		float angle = FMath::Acos(FVector::DotProduct(slope, horizontalSlope));

		wantedPitch = FMath::RadiansToDegrees(sign * angle);

		/*ToolBox::DEBUG_DRAW_POINT(GetWorld(), hitOuts[0].ImpactPoint);
		ToolBox::DEBUG_DRAW_POINT(GetWorld(), hitOuts[1].ImpactPoint);*/
		

	}

	mesh->SetRelativeRotation(FRotator(FMath::Lerp(r.Pitch, wantedPitch, deltaTime * 10),r.Yaw, r.Roll));
}

void UVisualComponent::AnimationGetLeftRightValue(const FInputActionValue& value)
{
	float amount = value.Get<float>();

	if (amount > 0) {
		animationInst->left = 0;
		animationInst->right = amount;
	}
	else if (amount < 0) {
		animationInst->right = 0;
		animationInst->left = abs(amount);
	}
	if (animationInst->speed < 0.25f) {
		animationInst->right = 0;
		animationInst->left = 0;

	}
}

