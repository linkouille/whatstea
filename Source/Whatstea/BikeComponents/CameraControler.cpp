#include "CameraControler.h"

#include "Whatstea/ToolBox.h"
#include "Whatstea/Inputs/InputHandler.h"

#include "GameFramework/SpringArmComponent.h"

// Sets default values for this component's properties
UCameraControler::UCameraControler()
{
	PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void UCameraControler::BeginPlay()
{
	Super::BeginPlay();

	cameraPivot = GetOwner()->FindComponentByClass<USpringArmComponent>();
	//cameraPivot->SetRelativeRotation(FRotator(0.f, GetOwner()->GetActorRotation().Yaw, -20.f));

	cameras[CameraState::TPS] = (UCameraComponent*)GetOwner()->GetComponentsByTag(UCameraComponent::StaticClass(), "Camera_TPS")[0];//  FindComponentByClass<UCameraComponent>();
	cameras[CameraState::FPS] = (UCameraComponent*)GetOwner()->GetComponentsByTag(UCameraComponent::StaticClass(), "Camera_FPS")[0];

	cameras[CameraState::FPS]->SetActive(false);

	currentState = CameraState::TPS;

	BindKeys();

}

// Called every frame
void UCameraControler::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// UpdateVisualEffects(DeltaTime);

	deltaTime = DeltaTime;

}

void UCameraControler::UpdateVisualEffects()
{
	float FOVstate = FMath::Lerp(FOVstate, wantedFOVValue, deltaTime * 2.0f);
	cameras[CameraState::TPS]->FieldOfView = FMath::Lerp(90.0f, cameraSpeedFOV, FOVstate > 1 ? 1 : FOVstate);

	cameras[CameraState::TPS]->PostProcessSettings.VignetteIntensity = FMath::Lerp(cameras[(int)CameraState::TPS]->PostProcessSettings.VignetteIntensity, wantedVignetteIntensity, deltaTime * 2.0f);
}

void UCameraControler::AddRotationYaw(const FInputActionValue& value)
{
	float amount = value.Get<float>();
	cameraPivot->AddWorldRotation(FRotator(0, amount * cameraSpeed * deltaTime, 0));
}

void UCameraControler::AddRotationPitch(const FInputActionValue& value)
{
	float amount = value.Get<float>();
	float newPitch = cameraPivot->GetComponentRotation().Pitch + amount * cameraSpeed * deltaTime;
	if (currentState == CameraState::TPS)
		cameraPivot->SetWorldRotation(FRotator(
			ToolBox::ClampValue<double>(
				cameraPivot->GetComponentRotation().Pitch + amount * (inverseY ? 1 : -1) * cameraSpeed * deltaTime, pitchClamp.X, pitchClamp.Y),
			cameraPivot->GetComponentRotation().Yaw,
			0));
	else if(canMoveFPS)
		cameras[currentState]->SetWorldRotation(FRotator(
			ToolBox::ClampValue<double>(
				cameras[currentState]->GetComponentRotation().Pitch + amount * (inverseY ? 1 : -1) * cameraSpeed * deltaTime, pitchClamp.X, pitchClamp.Y),
			cameras[currentState]->GetComponentRotation().Yaw,
			0));
}

void UCameraControler::AddRotationRoll(const FInputActionValue& value)
{
	float amount = value.Get<float>();
	if (currentState == CameraState::TPS)
		cameraPivot->AddWorldRotation(FRotator(0, 0, amount * cameraSpeed * deltaTime));
}

void UCameraControler::AlignRotationYaw()
{
	float yaw = cameraPivot->GetRelativeRotation().Yaw;
	if (!ToolBox::NearlyEquals(yaw, 0, 1)) {
		if(currentState == CameraState::TPS)
			cameraPivot->AddWorldRotation(FRotator(0, cameraAlignSpeed * (yaw < 0 ? 1 : -1), 0));
		else if (canMoveFPS)
			cameras[currentState]->AddWorldRotation(FRotator(0, cameraAlignSpeed * (yaw < 0 ? 1 : -1), 0));
	}
}

void UCameraControler::SetFOVValue(float value)
{
	wantedFOVValue = value;
}

void UCameraControler::SetVignetteIntensity(float intensity) {
	wantedVignetteIntensity = intensity;
}

void UCameraControler::ChangePOV() {
	//Flip flop
	
	cameras[currentState]->SetActive(false);
	currentState = currentState == CameraState::FPS ? CameraState::TPS : CameraState::FPS;
	cameras[currentState]->SetActive(true);


}

void UCameraControler::BindKeys() {
	UInputHandler* inputHandler = GetOwner()->FindComponentByClass<UInputHandler>();
	if (!inputHandler)
		ToolBox::DEBUG_ONSCREEN("ERROR: NO INPUTHANDLER ATTATCH");

	inputHandler->BindAction(UInputHandler::Cam_X, this, FName("AddRotationYaw"));
	
	inputHandler->BindAction(UInputHandler::Cam_Y, this, FName("AddRotationPitch"));

	inputHandler->BindAction(UInputHandler::Open_SB, this, FName("ChangePOV"));

}