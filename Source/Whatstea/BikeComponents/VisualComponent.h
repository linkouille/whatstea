#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Whatstea/Inputs/InputHandler.h"

#include "VisualComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class WHATSTEA_API UVisualComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere)
		FVector wheelsPos[2] = { FVector(52.0f, 0.0f, 0.0f),FVector(-46.0f, 0.0f, 0.0f) };

private:
	float wantedPitch;

	class USkeletalMeshComponent* mesh;

	class UBikeAnimationInstance* animationInst;

	class IMovement* movement;

	class UInputHandler* inputHandler;

public:
	UVisualComponent();

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void Setup(IMovement* p_movement);

protected:
	virtual void BeginPlay() override;

private:
	void UpdateAnimationValue(float deltaTime);

	void UpdateModelOrientation(float deltaTime);
		
	UFUNCTION()
		void AnimationGetLeftRightValue(const FInputActionValue& value);
};
