#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "BikeAnimationInstance.generated.h"

UCLASS()
class WHATSTEA_API UBikeAnimationInstance : public UAnimInstance
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		bool starting;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float speed;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float right;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float left;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float lean;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		bool sliding;

};
