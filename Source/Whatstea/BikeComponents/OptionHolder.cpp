#include "OptionHolder.h"
#include "Whatstea/ToolBox.h"
#include "Whatstea/HUD/Option/OptionHUD.h"

UOptionHolder::UOptionHolder()
{
	PrimaryComponentTick.bCanEverTick = true;

	static ConstructorHelpers::FClassFinder<UOptionHUD> HUDClassAsset(TEXT("/Game/Blueprints/HUD_Blueprint/Option/BP_OptionHUD"));
	if (HUDClassAsset.Succeeded()) {
		HUDClass = HUDClassAsset.Class;
	}

}


void UOptionHolder::BeginPlay()
{
	Super::BeginPlay();

	if (HUDClass) {
		HUD = CreateWidget<UOptionHUD>(GetWorld(), HUDClass);
		HUD->Setup();
		HUD->SetState(UOptionHUD::CONTROLS);
	}
}


void UOptionHolder::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

}

void UOptionHolder::ChangeHUDView()
{
	viewState = !viewState;

	if (viewState) {
		HUD->AddToPlayerScreen();
		inputHandler->ChangeMapping(UInputHandler::UI);
		movement->SetMove(false);
	}
	else {
		HUD->RemoveFromParent();
		inputHandler->ChangeMapping(UInputHandler::PLAYER);
		movement->SetMove(true);
	}

}

void UOptionHolder::ChangeCategory(const FInputActionValue& value)
{
	if (!viewState) return;

	float amount = value.Get<float>();

	if (amount > 0.1f) {
		HUD->PrevState();
	}
	else if (amount < -0.1f) {
		HUD->NextState();
	} 

}

void UOptionHolder::Select()
{
	if (!viewState) return;

	HUD->SelectCategory();
}

void UOptionHolder::Cancel()
{
	if (!viewState) return;
	HUD->QuitCategory();
}

void UOptionHolder::ChangeOptions(const FInputActionValue& value)
{
	if (!viewState) return;
	float amount = value.Get<float>();
	HUD->Activate(amount);
}

void UOptionHolder::Setup(IMovement* p_movement)
{
	inputHandler = GetOwner()->FindComponentByClass<UInputHandler>();
	if (!inputHandler)
		ToolBox::DEBUG_ONSCREEN("ERROR: NO INPUTHANDLER ATTATCH");

	inputHandler->BindAction(UInputHandler::Pause, this, "ChangeHUDView");
	inputHandler->BindAction(UInputHandler::MoveUI_Y_Once, this, "ChangeCategory");
	inputHandler->BindAction(UInputHandler::MoveUI_X_Once, this, "ChangeOptions");
	inputHandler->BindAction(UInputHandler::AcceptUI, this, "Select");
	inputHandler->BindAction(UInputHandler::CancelUI, this, "Cancel");

	movement = p_movement;

}

