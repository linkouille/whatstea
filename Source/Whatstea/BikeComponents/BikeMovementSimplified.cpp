#include "BikeMovementSimplified.h"

#include "Whatstea/BikeCharacter.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Whatstea/Inputs/InputHandler.h"
#include "Whatstea/ToolBox.h"

UBikeMovementSimplified::UBikeMovementSimplified()
{
	PrimaryComponentTick.bCanEverTick = true;

}

void UBikeMovementSimplified::BeginPlay()
{
	Super::BeginPlay();
}

void UBikeMovementSimplified::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	//Physics
	if (canMove) {

		UpdateStrenghts(DeltaTime);

		UpdateSlope(DeltaTime);

	}
}

void UBikeMovementSimplified::Setup(ABikeCharacter* p_character)
{
	character = p_character;

	initialSpeed = character->GetCharacterMovement()->MaxWalkSpeed;

	GetOwner()->FindComponentByClass<UCapsuleComponent>()->OnComponentHit.AddDynamic(this, &UBikeMovementSimplified::OnCollisionEnter);

	BindKeys();
}

float UBikeMovementSimplified::GetSpeedRatio()
{
	return strenght / maxStrenght;
}


void UBikeMovementSimplified::TurnRateInput(const FInputActionValue& value)
{
	float rate = value.Get<float>();
	if (canMove)
		character->AddControllerYawInput(rate * gamepadTurnRate * GetWorld()->GetDeltaSeconds());
}

void UBikeMovementSimplified::PedalInput_L(const FInputActionValue& value)
{
	float amount = value.Get<float>();
	if (strenght - brakeStrenght >= 0) {
		strenght -= brakeStrenght * amount;
	}
}

void UBikeMovementSimplified::PedalInput_R(const FInputActionValue& value)
{
	float amount = value.Get<float>();
	if (amount > 0.5 && !goForward) {
		goForward = true;
	} else if(amount < 0.5) {
		goForward = false;
	}
}


void UBikeMovementSimplified::BrakeInput()
{
	if (strenght - brakeStrenght >= 0) {
		strenght -= brakeStrenght;
	}
	else {
		strenght = 0;
	}

}

void UBikeMovementSimplified::ChangeCanMoveInput()
{
	canMove = !canMove;
	strenght = 0;
}

void UBikeMovementSimplified::BindKeys()
{
	UInputHandler* inputHandler = GetOwner()->FindComponentByClass<UInputHandler>();
	if (!inputHandler)
		ToolBox::DEBUG_ONSCREEN("ERROR: NO INPUTHANDLER ATTATCH");

	/*inputHandler->BindAxis(UInputMapping::Turn,
		this, &UBikeMovementSimplified::TurnRateInput);

	inputHandler->BindAxis(UInputMapping::Pedal_L,
		this, &UBikeMovementSimplified::PedalInput_L);

	inputHandler->BindAxis(UInputMapping::Pedal_R,
		this, &UBikeMovementSimplified::PedalInput_R);

	inputHandler->BindAction(UInputMapping::Open_SB,
		this, &UBikeMovementSimplified::ChangeCanMoveInput);
	*/
}


void UBikeMovementSimplified::UpdateSlope(float deltaTime)
{
	FHitResult hitOut;

	FVector start = character->GetActorLocation() + FVector::DownVector * character->GetCapsuleComponent()->GetScaledCapsuleHalfHeight();
	FVector end = start + character->GetActorUpVector() * -100;

	if (GetWorld()->LineTraceSingleByChannel(hitOut, start, end, ECC_Visibility)) {

		FVector slopeDirection = hitOut.ImpactNormal.Cross(hitOut.ImpactNormal.Cross(FVector::UpVector));

		float NdotF = FVector::DotProduct(hitOut.ImpactNormal, character->GetActorForwardVector());
		float sign = NdotF != 0 ? -NdotF / abs(NdotF) : 0;

		float projection = slopeDirection.Dot(character->GetActorForwardVector()) / character->GetActorForwardVector().Dot(character->GetActorForwardVector());

		isSloping = projection != 0;

		if (sign < 0 ) { // acc
			wantedMoveMult = slopeAccelerationMult;

		}
		else if (sign > 0 && -projection > 0.2f) { // drag
			wantedMoveMult = slopeDragMult;// *-projection;
		}
		else {
			wantedMoveMult = 1;
		}

	}
}


void UBikeMovementSimplified::UpdateStrenghts(float deltatime)
{
	//Udpate cumul strenght L and R	
	if (strenght < maxStrenght * 2 && goForward) {
		strenght += strenghtGain;
	}
	/*if (strenght > 0 && !goForward) {
		strenght -= dragStrenght;
	}*/

	/*ToolBox::DEBUG_ONSCREEN_F(strenght, "strenght", deltatime);
	ToolBox::DEBUG_ONSCREEN_F(strenght * moveMult, "Actual strenght", deltatime);*/

	moveMult = FMath::Lerp(moveMult, wantedMoveMult, 0.1f);

	// Apply Strenght Mult
	if (strenght / maxStrenght > 1)
		character->GetCharacterMovement()->MaxWalkSpeed = initialSpeed * (strenght * moveMult) / maxStrenght;

	// Apply Strenght
	const FRotator Rotation = character->Controller->GetControlRotation();
	const FRotator YawRotation(0, Rotation.Yaw, 0);
	const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
	character->AddMovementInput(Direction, (strenght * moveMult) / maxStrenght);
}

void UBikeMovementSimplified::OnCollisionEnter(UPrimitiveComponent* comp, AActor* otherActor, UPrimitiveComponent* otherComp, FVector normalImpulse, const FHitResult& hit)
{
	strenght = 0;
}