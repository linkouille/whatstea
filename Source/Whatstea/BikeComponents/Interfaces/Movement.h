#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"

#include "Whatstea/Inputs/InputHandler.h"

#include "Movement.generated.h"

UINTERFACE(MinimalAPI)
class UMovement : public UInterface
{
	GENERATED_BODY()
};

class WHATSTEA_API IMovement
{
	GENERATED_BODY()

protected:
	class ABikeCharacter* character;

	// current speed
	float strenght = 0;

	bool isSloping = false;

	//initial speed of the controler, used to accelerate controler if we go further 100% of speed
	float initialSpeed;

	bool canMove = true;
public:

	virtual void Setup(ABikeCharacter* p_character) {}

	virtual float GetSpeedRatio() { return 1.0f; }

	/// <summary>
	/// Turn the bike
	/// </summary>
	/// <param name="rate">How much we turn the bike, rate < 0 -> bike will go left & rate > 0 -> bike will go right</param>
	virtual void TurnRateInput(const FInputActionValue& value) {}

	/// <summary>
	/// Left pedal is pressed
	/// </summary>
	/// <param name="amount">How much we pressed the pedal (not used in strenth computing)</param>
	virtual void PedalInput_L(const FInputActionValue& value){}

	/// <summary>
	/// Right pedal is pressed
	/// </summary>
	/// <param name="amount">How much we pressed the pedal (not used in strenth computing)</param>
	virtual void PedalInput_R(const FInputActionValue& value){}

	/// <summary>
	/// Pressed Brake to stop the bike
	/// </summary>
	virtual void BrakeInput(){}

	/// <summary>
	/// Flip flop function to stop or resume character movement
	/// </summary>
	virtual void ChangeCanMoveInput(){}

	virtual void BindKeys(){}

	bool CanMove() { return canMove; }

	void SetMove(bool value) { canMove = value; }

	virtual bool IsPedaling() { return false; }

	virtual bool IsSloping() { return isSloping; }

private:
	// Update Functions	
	/// <summary>
	/// Apply slope influence on the bike
	/// </summary>
	/// <param name="deltaTime"></param>
	virtual void UpdateSlope(float deltaTime){}

	virtual void ApplyDrag(float deltaTime){}

	/// <summary>
	/// Update cumulated strenght in the pedals and then apply current strenght to the character in the bike direction
	/// </summary>
	/// <param name="deltaTime"></param>
	virtual void UpdateStrenghts(float deltaTime){}

};
