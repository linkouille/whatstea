
#include "Movement.h"

#include "Whatstea/BikeCharacter.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Whatstea/Inputs/InputHandler.h"
#include "Whatstea/ToolBox.h"

// Add default functionality here for any IMovement functions that are not pure virtual.
