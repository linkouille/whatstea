#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Interfaces/Movement.h"

#include "BikeMovementSimplified.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class WHATSTEA_API UBikeMovementSimplified : public UActorComponent, public IMovement
{
	GENERATED_BODY()

public:
	//Properties:
	UPROPERTY(EditAnywhere)
		float gamepadTurnRate = 50;

	UPROPERTY(EditAnywhere)
		float maxStrenght = 100;

	// How much pedal strenght grow
	UPROPERTY(EditAnywhere)
		float strenghtGain = 0.5;

	UPROPERTY(EditAnywhere)
		float brakeStrenght = 2;

	// How much the slope acceleration curve affect bike
	UPROPERTY(EditAnywhere)
		float slopeAccelerationMult = 2.0f;

	UPROPERTY(EditAnywhere)
		float slopeDragMult = 0.5f;

	// How much the drag curve affect bike
	UPROPERTY(EditAnywhere)
		float dragStrenght = 1;

private:
	class ABikeCharacter* character;

	bool goForward = false;

	float moveMult = 1;

	float wantedMoveMult;

	// bool pressedForward = false;

public:
	// Sets default values for this component's properties
	UBikeMovementSimplified();

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void Setup(ABikeCharacter* p_character);

	UFUNCTION(BlueprintCallable)
	float GetSpeedRatio();


	/// <summary>
	/// Left pedal is pressed
	/// </summary>
	/// <param name="amount">How much we pressed the pedal (not used in strenth computing)</param>
	void PedalInput_L(const FInputActionValue& value);

	/// <summary>
	/// Right pedal is pressed
	/// </summary>
	/// <param name="amount">How much we pressed the pedal (not used in strenth computing)</param>
	void PedalInput_R(const FInputActionValue& value);

	/// <summary>
	/// Pressed Brake to stop the bike
	/// </summary>
	void BrakeInput();

	/// <summary>
	/// Flip flop function to stop or resume character movement
	/// </summary>
	void ChangeCanMoveInput();

	/// <summary>
	/// Turn the bike
	/// </summary>
	/// <param name="rate">How much we turn the bike, rate < 0 -> bike will go left & rate > 0 -> bike will go right</param>
	virtual void TurnRateInput(const FInputActionValue& value);

	void BindKeys();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

private:
	// Update Functions	
	/// <summary>
	/// Apply slope influence on the bike
	/// </summary>
	/// <param name="deltaTime"></param>
	void UpdateSlope(float deltaTime);

	/// <summary>
	/// Update cumulated strenght in the pedals and then apply current strenght to the character in the bike direction
	/// </summary>
	/// <param name="deltaTime"></param>
	void UpdateStrenghts(float deltaTime);

	/// <summary>
	/// Callback function for when the character collide
	/// </summary>
	/// <param name="comp"></param>
	/// <param name="otherActor"></param>
	/// <param name="otherComp"></param>
	/// <param name="normalImpulse"></param>
	/// <param name="hit"></param>
	UFUNCTION()
		void OnCollisionEnter(UPrimitiveComponent* comp, AActor* otherActor, UPrimitiveComponent* otherComp, FVector normalImpulse, const FHitResult& hit);
};
