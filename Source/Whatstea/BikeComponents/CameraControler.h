#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

#include "Camera/CameraComponent.h"
#include "Whatstea/Inputs/InputHandler.h"

#include "CameraControler.generated.h"


/// <summary>
/// Actor Component that Handle character cameras
/// </summary>
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class WHATSTEA_API UCameraControler : public UActorComponent
{
	GENERATED_BODY()

public:
	/// <summary>
	/// All Camera state
	/// </summary>
	enum CameraState
	{
		FPS = 0,
		TPS,
		NB_STATE
	};

	// Camera rotation speed
	UPROPERTY(EditAnywhere)
		float cameraSpeed = 1.0f;

	// Pitch limitation for TPS Camera
	UPROPERTY(EditAnywhere)
		FVector2D pitchClamp = FVector2D(-80.0f,0.0f);

	// How fast camera Yaw move back to 0
	UPROPERTY(EditAnywhere)
		float cameraAlignSpeed = 0.5f;

	// Camera Max FOV
	UPROPERTY(EditAnywhere)
		float cameraSpeedFOV = 120.0f;

	// Inverse Camera control for pitch
	UPROPERTY(EditAnywhere)
		bool inverseY;

private:

	// All cameras
	UCameraComponent* cameras[NB_STATE];

	// Current Camera redering
	CameraState currentState;

	// TPS Camera Pivot
	class USpringArmComponent* cameraPivot;

	// FOV wanted for camera
	float wantedFOVValue;

	// Current Vignette Intensity (not used)
	float wantedVignetteIntensity;

	// Can the player move while in FPS mode
	bool canMoveFPS = false;

	float deltaTime;

public:
	// Sets default values for this component properties
	UCameraControler();

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	/// <summary>
	/// Add amount to world rotation Yaw
	/// </summary>
	UFUNCTION()
		void AddRotationYaw(const FInputActionValue& value);

	/// <summary>
	/// Add amount to world rotation Pitch
	/// </summary>
	UFUNCTION()
		void AddRotationPitch(const FInputActionValue& value);

	/// <summary>
	/// Add amount to world rotation Roll
	/// </summary>
	UFUNCTION()
		void AddRotationRoll(const FInputActionValue& value);

	/// <summary>
	/// Snap back Yaw to 0
	/// </summary>
	void AlignRotationYaw();

	void SetFOVValue(float value);
	
	void SetVignetteIntensity(float intensity);

	/// <summary>
	/// Flip Flop function to switch from TPS to FPS
	/// </summary>
	UFUNCTION()
		void ChangePOV();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	/// <summary>
	/// Update FOV and Post process value, called every frame
	/// </summary>
	void UpdateVisualEffects();

	/// <summary>
	/// Bind functions to Actions (see InputHandler)
	/// </summary>
	void BindKeys();

};
