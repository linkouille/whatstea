// Copyright Epic Games, Inc. All Rights Reserved.

#include "WhatsteaGameMode.h"
#include "WhatsteaCharacter.h"
#include "UObject/ConstructorHelpers.h"

AWhatsteaGameMode::AWhatsteaGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/BP_Bike"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
