#include "ToolBox.h"

void ToolBox::DEBUG_ONSCREEN(const FString& content, float time, FColor color)
{
	GEngine->AddOnScreenDebugMessage(-1, time, color,content);
}

void ToolBox::DEBUG_ONSCREEN_ERROR(const FString& content)
{
	DEBUG_ONSCREEN(content,5.0f,FColor::Red);
}

void ToolBox::DEBUG_ONSCREEN_F(float var, const FString& name, float time, FColor color)
{
	DEBUG_ONSCREEN(FString::Printf(TEXT("%s: %f"), *name, var),time,color);
}

void ToolBox::DEBUG_DRAW_RAY(const UWorld * world, FVector start, FVector dir, FColor color)
{
	DrawDebugLine(world, start, start + dir * RAY_LEN, color, false, 1, 1);
}

void ToolBox::DEBUG_DRAW_POINT(const UWorld* world, FVector pos, FColor color) {
	DrawDebugSphere(world, pos, POINT_SIZE, 8, color);
}

void ToolBox::DEBUG_DRAW_SPHERE(const UWorld* world, FVector pos, float radius, FColor color)
{
	DrawDebugSphere(world, pos, radius, 16, color,false,1.0f);
}
