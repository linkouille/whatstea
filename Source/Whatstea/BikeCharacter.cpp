#include "BikeCharacter.h"

#include "Components/ActorComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/PrimitiveComponent.h"
#include "Whatstea/BikeComponents/CameraControler.h"
#include "Whatstea/Inputs/InputHandler.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Whatstea/ToolBox.h"
#include "Whatstea/Dialogues/Interfaces/Interactable.h"
#include "Whatstea/Collections/Collectable.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Whatstea/HUD/CollectableHUD.h"
#include "UObject/ConstructorHelpers.h"
#include "GameData.h"
#include "Whatstea/BikeComponents/VisualComponent.h"
#include "Whatstea/Dialogues/DialoguesHandler.h"
#include "Whatstea/ScrapBook/ScrapBookHandler.h"
#include "Whatstea/BikeComponents/OptionHolder.h"

#include "Whatstea/BikeComponents/BikeMovement.h"
#include "Whatstea/BikeComponents/BikeMovementSimplified.h"

// Sets default values
ABikeCharacter::ABikeCharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ABikeCharacter::BeginPlay()
{
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	cameraControler = AActor::FindComponentByClass<UCameraControler>();
	if (!cameraControler)
		ToolBox::DEBUG_ONSCREEN("ERROR: NO CAMERACONTROLER ATTATCH");

	inputHandler = AActor::FindComponentByClass<UInputHandler>();
	if (!inputHandler)
		ToolBox::DEBUG_ONSCREEN("ERROR: NO INPUTHANDLER ATTATCH");

	inputHandler->Setup(playerInputComponent, Cast<APlayerController>(GetController()));

	SetupBikeMovement();

	//Bind Interaction
	inputHandler->BindAction(UInputHandler::Interact, this, FName("TryInteract"));

	// Setup SphereCast Variables
	traceObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECollisionChannel::ECC_WorldDynamic));
	ignoreActors.Add(this);

	gameData = AActor::FindComponentByClass<UGameData>();
	if (!gameData)
		ToolBox::DEBUG_ONSCREEN("ERROR: NO GAMEDATA ATTATCH");

	visualComponent = AActor::FindComponentByClass<UVisualComponent>();
	if (!visualComponent)
		ToolBox::DEBUG_ONSCREEN_ERROR("ERROR: NO VISUAL COMPONENT ATTATCH");
	visualComponent->Setup(movement);

	dialoguesHandler = AActor::FindComponentByClass<UDialoguesHandler>();
	if (!dialoguesHandler)
		ToolBox::DEBUG_ONSCREEN_ERROR("ERROR: NO DIALOGUE HANDLER ATTATCH");
	dialoguesHandler->characterMovement = movement;

	scrapBookHandler = AActor::FindComponentByClass<UScrapBookHandler>();
	if (!scrapBookHandler)
		ToolBox::DEBUG_ONSCREEN_ERROR("ERROR: NO SCRAPBOOK HANDLER ATTATCH");
	scrapBookHandler->Setup();

	optionHolder = AActor::FindComponentByClass<UOptionHolder>();
	if (!optionHolder)
		ToolBox::DEBUG_ONSCREEN_ERROR("ERROR: NO OPTION HOLDER ATTATCH");
	optionHolder->Setup(movement);

	Super::BeginPlay();
}

// Called every frame
void ABikeCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//Visuals
	UpdateCamera(DeltaTime);

	UpdateModel(DeltaTime);

}

// Called to bind functionality to input
void ABikeCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	playerInputComponent = PlayerInputComponent;
}

void ABikeCharacter::UpdateCamera(float deltatTime)
{
	if (!cameraControler)
		return;

	if( movement->GetSpeedRatio() > 0.5f ) cameraControler->AlignRotationYaw();
	//cameraControler->SetFOVValue(strenght / maxStrenght);

}

void ABikeCharacter::SetupBikeMovement()
{
	if (simplifiedMovement) {
		movement = AActor::FindComponentByClass<UBikeMovementSimplified>();
	}
	else {
		movement = AActor::FindComponentByClass<UBikeMovement>();
	}
	if (!movement) {
		ToolBox::DEBUG_ONSCREEN("ERROR: NO BIKE MOVEMENT", 5.0f, FColor::Red);
		return;
	}
	movement->Setup(this);
}

void ABikeCharacter::TryInteract()
{

	//ToolBox::DEBUG_DRAW_SPHERE(GetWorld(), GetActorLocation(), activateDetectionRange, FColor::Red);

	if (UKismetSystemLibrary::SphereOverlapActors(GetWorld(), GetActorLocation(), activateDetectionRange, traceObjectTypes, NULL, ignoreActors, outActors)) {
		
		IInteractable* interactable = Cast<IInteractable>(outActors[0]);
		if (interactable) {
			interactable->Interact(dialoguesHandler->_getUObject());
		}

		ICollectable* collectable = Cast<ICollectable>(outActors[0]);
		if (collectable) {
			collectable->Gather(gameData);
		}

	}
}


void ABikeCharacter::UpdateModel(float deltaTime)
{

}



