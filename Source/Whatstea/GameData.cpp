#include "GameData.h"
#include "Whatstea/HUD/CollectableHUD.h"
#include "ToolBox.h"

UGameData::UGameData()
{
	PrimaryComponentTick.bCanEverTick = false;

	static ConstructorHelpers::FClassFinder<UCollectableHUD> collectableHUDClassAsset(*collectableHUDClassPath);
	if (collectableHUDClassAsset.Succeeded()) {
		collectableHUDClass = collectableHUDClassAsset.Class;
	}

	teaInventory = new int[NB_TEATYPE];
	for (size_t i = 0; i < NB_TEATYPE; i++) teaInventory[i] = 0;

	//TEST To Remove
	teaInventory[0] = 1;
	teaInventory[2] = 2;

	subjectInventory.Add("An old photo of Jay and Kevin (good friend)");
	subjectInventory.Add("Friend Chocobon");
	/*FString csvFile = ReadCSV("Dialogues/Test.csv");
	ToolBox::DEBUG_ONSCREEN(csvFile, 5.0f);*/
	
}

FString UGameData::ReadCSV(FString path)
{

	FString file = FPaths::ProjectContentDir();
	file.Append(path);

	FString fileContent;

	IPlatformFile& FileManager = FPlatformFileManager::Get().GetPlatformFile();
	if (FileManager.FileExists(*file))
	{
		if (FFileHelper::LoadFileToString(fileContent, *file, FFileHelper::EHashOptions::None))
		{
			return fileContent;
		}
		else {
			ToolBox::DEBUG_ONSCREEN("FAILED READING FILE", 4, FColor::Red);
		}
	}
	else {
		ToolBox::DEBUG_ONSCREEN("NO file", 4, FColor::Red);

	}

	return "";
}

void UGameData::BeginPlay()
{
	Super::BeginPlay();

	if (collectableHUDClass) {
		collectableHUD = CreateWidget<UCollectableHUD>(GetWorld(), collectableHUDClass);
		if (collectableHUD) {
			collectableHUD->gameData = this;
			collectableHUD->AddToPlayerScreen();

		}
	}
}

