#pragma once

#include "CoreMinimal.h"

class WHATSTEA_API DialogueElement
{

public:
	int id;
	FString content;
	int next;
	FString condition; // NOT IMPLEMENTED YET

public:
	DialogueElement(int p_id, FString p_content, int p_next, FString p_condition);
	~DialogueElement();
};
