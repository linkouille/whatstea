#include "DialoguesHandler.h"

#include "Components/SphereComponent.h"
#include "Whatstea/ToolBox.h"
#include "UObject/ConstructorHelpers.h"
#include "Whatstea/BikeCharacter.h"
#include "Whatstea/BikeComponents/Interfaces/Movement.h"
#include "Components/Image.h"
#include "Components/TextBlock.h"
#include "Math/Color.h"
#include "Whatstea/HUD/Dialogues/ChooseTeaHUD.h"
#include "Whatstea/HUD/Dialogues/DialogueHUD.h"

UDialoguesHandler::UDialoguesHandler()
{
	PrimaryComponentTick.bCanEverTick = true;

	static ConstructorHelpers::FClassFinder<UChooseTeaHUD> chooseClassAsset(TEXT("/Game/Blueprints/HUD_Blueprint/Dialogue/BP_ChooseTeaHUD"));
	if (chooseClassAsset.Succeeded()) {
		chooseTeaHUDClass = chooseClassAsset.Class;
	}

	static ConstructorHelpers::FClassFinder<UDialogueHUD> dialogueClassAsset(TEXT("/Game/Blueprints/HUD_Blueprint/Dialogue/BP_DialogueHUD"));
	if (chooseClassAsset.Succeeded()) {
		dialogueHUDClass = dialogueClassAsset.Class;
	}
}

void UDialoguesHandler::BeginPlay()
{
	Super::BeginPlay();

	if (dialogueHUDClass) {
		dialogueHUD = CreateWidget<UDialogueHUD>(GetWorld(), dialogueHUDClass);
	}
	dialogueHUD->dialoguehandler = this;

	if (chooseTeaHUDClass) {
		chooseTeaHUD = CreateWidget<UChooseTeaHUD>(GetWorld(), chooseTeaHUDClass);
	}
	chooseTeaHUD->Setup(GetOwner(), this);

	BindInput();
}

void UDialoguesHandler::BindInput()
{
	inputHandler = GetOwner()->FindComponentByClass<UInputHandler>();
	if (!inputHandler) {
		ToolBox::DEBUG_ONSCREEN("ERROR: NO INPUTHANDLER ATTATCH");
		return;
	}

	inputHandler->BindAction(UInputHandler::AcceptUI, this, "Accept");
	inputHandler->BindAction(UInputHandler::AcceptUI_Released, this, "AcceptRelase");
	inputHandler->BindAction(UInputHandler::CancelUI, this, "Cancel");
	inputHandler->BindAction(UInputHandler::MoveUI_X, this, "MoveX");
	inputHandler->BindAction(UInputHandler::MoveUI_Y, this, "MoveY");
}

void UDialoguesHandler::Cancel()
{
	if (state == Choose) Close();
}

void UDialoguesHandler::Accept()
{
	if(state == Choose) chooseTeaHUD->Accept();
	if(state == Dialogue) dialogueHUD->Accept();
}

void UDialoguesHandler::AcceptRelase()
{
	if (state == Dialogue) dialogueHUD->Accept();
}

void UDialoguesHandler::MoveX(const FInputActionValue& value)
{
	if (state == Choose) chooseTeaHUD->MoveX(value.Get<float>());
}

void UDialoguesHandler::MoveY(const FInputActionValue& value)
{
	if (state == Choose) chooseTeaHUD->MoveY(value.Get<float>());
}

void UDialoguesHandler::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (state == Dialogue) dialogueHUD->Update(DeltaTime);
}

void UDialoguesHandler::StartNewDialogue()
{
	if (state == Choose) {
		chooseTeaHUD->Hide();
		
		dialogueHUD->SetDialogueContent(dialogueContent);

		dialogueHUD->StartNewDialogue();
		state = Dialogue;

	}
}

void UDialoguesHandler::StartContext()
{
	ToolBox::DEBUG_ONSCREEN("Start Context");
	inputHandler->ChangeMapping(UInputHandler::UI);
	characterMovement->SetMove(false);

	chooseTeaHUD->Screen();

	state = Choose;
}

void UDialoguesHandler::Close()
{
	inputHandler->ChangeMapping(UInputHandler::PLAYER);
	characterMovement->SetMove(true);
	chooseTeaHUD->Hide();
	dialogueHUD->RemoveFromParent();
	state = None;
	// Add to GameData if
}
