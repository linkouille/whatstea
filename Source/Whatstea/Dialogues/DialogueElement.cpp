#include "DialogueElement.h"

DialogueElement::DialogueElement(int p_id, FString p_content, int p_next, FString p_condition)
{
	id = p_id;
	content = p_content;
	next = p_next;
	condition = p_condition;
}

DialogueElement::~DialogueElement()
{
}
