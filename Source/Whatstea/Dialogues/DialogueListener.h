#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interfaces/Interactable.h"
#include "Whatstea/HUD/Dialogues/DialogueHUD.h"

#include "DialogueListener.generated.h"

UCLASS()
class WHATSTEA_API ADialogueListener : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:

	UPROPERTY(EditAnywhere)
		FText pnjName;

	UPROPERTY(EditAnywhere)
		FLinearColor pnjColor;

	UPROPERTY(EditAnywhere)
		FText textContent;


public:	
	// Sets default values for this actor's properties
	ADialogueListener();

	void Tick(float DeltaTime) override;

	void Interact(UObject* other);


protected:
	// Called when the game starts or when spawned
	void BeginPlay() override;


};
