#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Whatstea/Inputs/InputHandler.h"
#include "DialoguesHandler.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class WHATSTEA_API UDialoguesHandler : public UActorComponent
{
	GENERATED_BODY()


public:
	UPROPERTY()
		class UDialogueHUD* dialogueHUD;

	UPROPERTY()
		class UChooseTeaHUD* chooseTeaHUD;

	UPROPERTY(EditAnywhere)
		float textSpeed = 0.1f;

	class IMovement* characterMovement;

	FString dialogueContent;

private:
	enum DialogueHandlerState
	{
		None,
		Choose,
		Dialogue
	};

	DialogueHandlerState state = None;

	TSubclassOf<UChooseTeaHUD> chooseTeaHUDClass;
	TSubclassOf<UDialogueHUD> dialogueHUDClass;

	UInputHandler* inputHandler;

	//FString tmp = "Hello traveler! |Have you seen the view? The cloud sea? Quite breathtaking| Someone told me it was all trees, to know you will have to go down the hill. |Me? I can't, I'm too afraid...";

public:

	UDialoguesHandler();

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void StartNewDialogue();

	void StartContext();

	void Close();

protected:

	virtual void BeginPlay() override;

private:

	void BindInput();
	
	UFUNCTION()
		void Cancel();

	UFUNCTION()
		void Accept();

	UFUNCTION()
		void AcceptRelase();

	UFUNCTION()
		void MoveX(const FInputActionValue& value);

	UFUNCTION()
		void MoveY(const FInputActionValue& value);
};
