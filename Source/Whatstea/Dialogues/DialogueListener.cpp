#include "DialogueListener.h"

#include "Components/SphereComponent.h"
#include "Whatstea/ToolBox.h"
#include "UObject/ConstructorHelpers.h"
#include "Whatstea/BikeCharacter.h"
#include "Whatstea/BikeComponents/Interfaces/Movement.h"
#include "Components/Image.h"
#include "Components/TextBlock.h"
#include "Math/Color.h"
#include "Whatstea/HUD/Dialogues/ChooseTeaHUD.h"
#include "Components/InputComponent.h"
#include "Whatstea/Dialogues/DialoguesHandler.h"


// Sets default values
ADialogueListener::ADialogueListener()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ADialogueListener::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ADialogueListener::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ADialogueListener::Interact(UObject* other)
{

	UDialoguesHandler* dialogueHandler = Cast<UDialoguesHandler>(other);
	if (!dialogueHandler) {
		ToolBox::DEBUG_ONSCREEN_ERROR("Failed Cast Dialogue handler");
		return;
	}

	dialogueHandler->dialogueHUD->Name->SetText(pnjName);
	dialogueHandler->dialogueHUD->Name->SetColorAndOpacity(pnjColor);
	dialogueHandler->dialogueContent = textContent.ToString();

	dialogueHandler->StartContext();
	

}

