#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Components/Image.h"
#include "ScrapBookHandler.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class WHATSTEA_API UScrapBookHandler : public UActorComponent
{
	GENERATED_BODY()

public:

	UPROPERTY()
		class UScrapBookHUD* HUD;

private:
	TSubclassOf<class UScrapBookHUD> HUDClass;

	FVector2D cursorVelocity = FVector2D(0);

	bool state = false;

	class UInputHandler* inputHandler;

	TArray<UImage*> placedElement;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Sets default values for this component's properties
	UScrapBookHandler();
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void DisplayHUD();

	void HideHUD();

	void BindKeys();

	void Setup();

	UFUNCTION()
		void SwitchView();

};
