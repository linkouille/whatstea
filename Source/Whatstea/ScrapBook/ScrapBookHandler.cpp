#include "ScrapBookHandler.h"

#include "Whatstea/HUD/ScrapBook/ScrapBookHUD.h"
#include "Blueprint/UserWidget.h"
#include "Whatstea/ToolBox.h"
#include "Whatstea/Inputs/InputHandler.h"

#include "Components/CanvasPanelSlot.h"
#include "Components/PanelWidget.h"
#include "Blueprint/WidgetTree.h"
#include "Components/CanvasPanel.h"

// Sets default values for this component's properties
UScrapBookHandler::UScrapBookHandler()
{
	PrimaryComponentTick.bCanEverTick = true;
	static ConstructorHelpers::FClassFinder<UScrapBookHUD> scrapBookHUDClassAsset(TEXT("/Game/Blueprints/HUD_Blueprint/ScrapBook/BP_ScrapBookHUD"));
	if (scrapBookHUDClassAsset.Succeeded()) {
		HUDClass = scrapBookHUDClassAsset.Class;
	}
}

// Called when the game starts
void UScrapBookHandler::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void UScrapBookHandler::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void UScrapBookHandler::DisplayHUD()
{
	HUD->AddToPlayerScreen();
}

void UScrapBookHandler::HideHUD()
{
	HUD->RemoveFromParent();
}

void UScrapBookHandler::BindKeys()
{
	inputHandler = GetOwner()->FindComponentByClass<UInputHandler>();
	if (!inputHandler)
		ToolBox::DEBUG_ONSCREEN("ERROR: NO INPUTHANDLER ATTATCH");

	inputHandler->BindAction(UInputHandler::Open_SB, this, "SwitchView");
	inputHandler->BindAction(UInputHandler::NextMenu, HUD, "NextState");
	inputHandler->BindAction(UInputHandler::PrevMenu, HUD, "PrevState");
}

void UScrapBookHandler::Setup()
{
	if (HUDClass) {
		HUD = CreateWidget<UScrapBookHUD>(GetWorld(), HUDClass);
	}
	BindKeys();
	//SwitchView();
}

void UScrapBookHandler::SwitchView()
{

	if (state) {
		HideHUD();
		inputHandler->ChangeMapping(UInputHandler::PLAYER);
	}
	else {
		DisplayHUD();
		inputHandler->ChangeMapping(UInputHandler::UI);

	}

	state = !state;
}
