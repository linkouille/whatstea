#pragma once

#include "CoreMinimal.h"

#include "GameFramework/Character.h"
#include "Whatstea/BikeComponents/Interfaces/Movement.h"

#include "BikeCharacter.generated.h"

/// <summary>
/// Bike controler that handle character movement and for now some model animations 
/// </summary>
UCLASS()
class WHATSTEA_API ABikeCharacter : public ACharacter
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere)
		bool simplifiedMovement = false;

	UPROPERTY(EditAnywhere)
		float activateDetectionRange = 500.0f;

private:

	// Camera controler used to update FOV based on current speed 
	class UCameraControler * cameraControler;

	// Used to bind input action to method, is initialised here
	class UInputHandler * inputHandler;

	class UVisualComponent* visualComponent;

	class UDialoguesHandler* dialoguesHandler;

	class UScrapBookHandler* scrapBookHandler;

	class UOptionHolder* optionHolder;

	// temporary value to send to inputHandler
	UInputComponent* playerInputComponent;

	IMovement* movement;

	// Pitch we want for model
	float wantedPitch;

	class UGameData* gameData;

	// SphereCast Varaibles
	TArray<TEnumAsByte<EObjectTypeQuery>> traceObjectTypes;

	TArray<AActor*> ignoreActors;

	TArray<AActor*> outActors;

	// Activate debug
	bool DEBUG_BIKE_VALUES = false;

public:
	// Sets default values for this character's properties
	ABikeCharacter();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	/// <summary>
	/// (LEGACY) Bind method to input actions see InputHandler
	/// </summary>
	// void BindPlayerInput();

	/// <summary>
	/// (TEMP) Update model orientation
	/// </summary>
	/// <param name="deltaTime"></param>
	void UpdateModel(float deltaTime);


	/// <summary>
	/// Update Camera effects based on characters values (speed, orientation...)
	/// </summary>
	/// <param name="deltatTime"></param>
	void UpdateCamera(float deltatTime);

	void SetupBikeMovement();

	UFUNCTION()
		void TryInteract();


};
