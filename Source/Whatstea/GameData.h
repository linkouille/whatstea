#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Whatstea/Dialogues/DialogueElement.h"

#include "GameData.generated.h"


UENUM(BlueprintType)
enum TeaType
{
	ENTHUSIASTIC,
	DRAMATIC,
	INTIMATE,
	PLAYFUL,
	AROUSING,
	CONFUSING,
	NB_TEATYPE
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class WHATSTEA_API UGameData : public UActorComponent
{
	GENERATED_BODY()


public:
	int * teaInventory;
	TArray<FString> subjectInventory;

	UPROPERTY(EditAnywhere)
		FString teaNames[NB_TEATYPE] = {
			"Overly Enthusiastic Tea",
			"Overly Dramatic Tea",
			"Overly Intimate Tea",
			"Overly Playful Tea",
			"Overly Arousing Tea",
			"OVerly Confusing Tea"	
		};

	UPROPERTY(EditAnywhere)
		FString collectableHUDClassPath = "/Game/Blueprints/HUD_Blueprint/BP_CollectableHUD";

	UPROPERTY()
		class UCollectableHUD* collectableHUD = nullptr;

private:
	TSubclassOf<class UCollectableHUD> collectableHUDClass;

public:	
	UGameData();

	FString ReadCSV(FString path);

protected:
	virtual void BeginPlay() override;

};