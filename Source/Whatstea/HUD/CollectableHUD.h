#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Whatstea/GameData.h"

#include "CollectableHUD.generated.h"

UCLASS()
class WHATSTEA_API UCollectableHUD : public UUserWidget
{
	GENERATED_BODY()


public:
	UPROPERTY(EditAnywhere, meta = (BindWidget))
		class UPanelWidget* NotifStack;

	UPROPERTY()
		UGameData* gameData;

private:
	TArray<class UCollectEventHUD *> eventElements;

	int currentElement;

	FWidgetAnimationDynamicEvent popEvent;

public:

	void AddEvent(TeaType type);
	bool Initialize();

	UFUNCTION()
		void PopNotification();

	void PostLoad();

};
