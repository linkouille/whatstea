#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "CollectEventHUD.generated.h"

UCLASS()
class WHATSTEA_API UCollectEventHUD : public UUserWidget
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, meta = (BindWidget))
		class UTextBlock* CollectableName;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
		class UTextBlock* CollectableCount;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
		class UImage* CollectableIcon;


	UPROPERTY(Transient, meta = (BindWidgetAnim))
		UWidgetAnimation* Idle;

};
