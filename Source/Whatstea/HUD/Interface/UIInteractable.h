#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "UIInteractable.generated.h"

UINTERFACE(MinimalAPI)
class UUIInteractable : public UInterface
{
	GENERATED_BODY()
};

class WHATSTEA_API IUIInteractable
{
	GENERATED_BODY()


protected:
	bool selected;
	bool hover;

public:

	virtual void SetSelected(bool state) {
		selected = true;
	}

	virtual void SetHover(bool state) {
		hover = true;
	}

	virtual bool IsComboBox() { return false; }
	virtual bool IsSlider() { return false; }
};
