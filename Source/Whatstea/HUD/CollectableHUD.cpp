#include "CollectableHUD.h"
#include "CollectEventHUD.h"
#include "Whatstea/ToolBox.h"
#include "Components/PanelWidget.h"
#include "Blueprint/WidgetTree.h"
#include "Animation/WidgetAnimation.h"
#include "Components/TextBlock.h"
#include "Whatstea/GameData.h"


void UCollectableHUD::PostLoad()
{
	Super::PostLoad();
}

void UCollectableHUD::AddEvent(TeaType type)
{
	if (currentElement > eventElements.Num()) {
		return;
	}
	UCollectEventHUD* eventElement = eventElements[currentElement];
	eventElement->PlayAnimation(eventElement->Idle, 0.0f, 1);

	eventElement->CollectableName->SetText(FText::FromString(gameData->teaNames[(int)type]));
	eventElement->CollectableCount->SetText(FText::FromString(FString::Printf(TEXT(" %d"), gameData->teaInventory[(int)type])));

	currentElement++;
}

bool UCollectableHUD::Initialize()
{
	bool output = Super::Initialize();
	
	TArray<UWidget*> widgetChildList;
	WidgetTree->GetChildWidgets(NotifStack, widgetChildList);

	UCollectEventHUD* tempColectEvent;
	for (UWidget* c : widgetChildList)
	{
		tempColectEvent = Cast<UCollectEventHUD>(c);
		if (tempColectEvent) {
			eventElements.Add(tempColectEvent);
		}
		tempColectEvent = nullptr;
	}

	popEvent.BindDynamic(this, &UCollectableHUD::PopNotification);
	for (UCollectEventHUD* eventElement : eventElements) {
		eventElement->BindToAnimationFinished(eventElement->Idle, popEvent);
	}

	return output;
}

void UCollectableHUD::PopNotification()
{
	//Called when animation end
	currentElement--;
}
