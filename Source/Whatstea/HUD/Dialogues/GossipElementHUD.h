#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "GossipElementHUD.generated.h"

UCLASS()
class WHATSTEA_API UGossipElementHUD : public UUserWidget
{
	GENERATED_BODY()

public:
	int type;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
		class UPanelWidget* ElementPanel;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
		class UTextBlock* SubjectText;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
		class UPanelWidget* SelectedPanel;

private:
	bool selected;
	bool hover;

public:

	void SetSelected(bool state);

	void SetHover(bool state);
};
