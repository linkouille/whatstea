#include "DialogueHUD.h"
#include "Components/TextBlock.h"
#include "Whatstea/ToolBox.h"

void UDialogueHUD::StartNewDialogue()
{
	charCount = 0;
	timeCount = 0;
	textBuffer = "";
	DialogueContent->SetText(FText::FromString(""));

	AddToPlayerScreen();
	state = Running;
}

void UDialogueHUD::SetTextSpeed(float value)
{
	textSpeed = value;
}

void UDialogueHUD::SetDialogueContent(FString& text)
{
	dialogueText = text;
}

void UDialogueHUD::Update(float deltaTime)
{
	UpdateTime(deltaTime);

	UpdateState(deltaTime);
}

void UDialogueHUD::UpdateTime(float deltaTime)
{
	timeCount += deltaTime;
	if (timeCount > textSpeed) {
		timeTrigger = true;
		timeCount = 0.f;
	}
}

void UDialogueHUD::UpdateState(float deltaTime)
{
	if (state == Running) {
		if (timeTrigger) {
			if (charCount == dialogueText.Len() - 1) { 
				state = End; 
				return;
			}
			if (dialogueText[charCount] == '|') { 
				charCount++;
				state = Pause; 

			} else {
				textBuffer.AppendChar(dialogueText[charCount]);
				DialogueContent->SetText(FText::FromString(textBuffer));
				charCount++;
			}
			timeTrigger = false;
		}
	}
	else if (state == Fast) {
		if (charCount == dialogueText.Len() - 1) {
			state = End;
			return;
		}
		if (dialogueText[charCount] == '|') {
			charCount++;
			state = Pause;

		}
		else {
			textBuffer.AppendChar(dialogueText[charCount]);
			DialogueContent->SetText(FText::FromString(textBuffer));
			charCount++;
		}
	}
	
}

void UDialogueHUD::Accept()
{
	switch (state)
	{
	case UDialogueHUD::Start:
		break;
	case UDialogueHUD::Running:
		state = Fast;
		break;
	case UDialogueHUD::Fast:
		state = Running;
		break;
	case UDialogueHUD::Pause:
		textBuffer = "";
		state = Running;
		break;
	case UDialogueHUD::End:
		// Close
		dialoguehandler->Close();
		break;
	default:
		break;
	}
}
