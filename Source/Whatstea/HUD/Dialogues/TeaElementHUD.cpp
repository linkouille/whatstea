#include "TeaElementHUD.h"
#include "Components/Image.h"
#include "Components/TextBlock.h"
#include "Components/Button.h"
#include "Components/PanelWidget.h"

void UTeaElementHUD::SetSelected(bool state)
{
	selected = state;
	
}

void UTeaElementHUD::SetHover(bool state)
{
	hover = state;
	SelectedPanel->RenderOpacity = state ? 1.f : 0.f;
	SelectedPanel->SynchronizeProperties();
}

void UTeaElementHUD::SetTea(TeaType p_type, UGameData* gameData)
{
	TeaText->SetText(FText::FromString(gameData->teaNames[(int)p_type]));
	FString quantityString = FString::Printf(TEXT("%d"), gameData->teaInventory[(int)p_type]);
	QuantityText->SetText(FText::FromString(quantityString));
	type = p_type;
}
