#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Whatstea/GameData.h"
#include "TeaElementHUD.generated.h"

UCLASS()
class WHATSTEA_API UTeaElementHUD : public UUserWidget
{
	GENERATED_BODY()
	
public:
	TeaType type;

	UPROPERTY(EditAnywhere)
		FColor selectedColor;

	UPROPERTY(EditAnywhere)
		FColor hoverColor;

	UPROPERTY(EditAnywhere)
		FColor baseColor;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
		class UPanelWidget* ElementPanel;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
		class UTextBlock* TeaText;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
		class UTextBlock* QuantityText;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
		class UImage* ElementImage;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
		class UPanelWidget* SelectedPanel;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
		class UImage* ImageCadre;

private:
	bool selected;
	bool hover;

public:

	void SetSelected(bool state);

	void SetHover(bool state);

	void SetTea(TeaType p_type, UGameData * gameData);
};
