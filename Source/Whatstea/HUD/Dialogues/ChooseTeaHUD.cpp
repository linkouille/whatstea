#include "ChooseTeaHUD.h"

#include "TeaElementHUD.h"
#include "GossipElementHUD.h"
#include "Components/Image.h"
#include "Components/TextBlock.h"
#include "Components/Button.h"
#include "Components/PanelWidget.h"
#include "Whatstea/ToolBox.h"

UChooseTeaHUD::UChooseTeaHUD(const FObjectInitializer& ObjectInitializer) : UUserWidget(ObjectInitializer)
{
	// Load class
	static ConstructorHelpers::FClassFinder<UTeaElementHUD> teaElementHUDClassAsset(TEXT("/Game/Blueprints/HUD_Blueprint/Dialogue/BP_TeaElementHUD"));
	if (teaElementHUDClassAsset.Succeeded()) {
		teaElementHUDClass = teaElementHUDClassAsset.Class;
	}
	static ConstructorHelpers::FClassFinder<UGossipElementHUD> subjectElementHUDClassAsset(TEXT("/Game/Blueprints/HUD_Blueprint/Dialogue/BP_GossipElementHUD"));
	if (subjectElementHUDClassAsset.Succeeded()) {
		subjectElementHUDClass = subjectElementHUDClassAsset.Class;
	}

}

void UChooseTeaHUD::Setup(UObject* other, UDialoguesHandler* p_dialogueHandler)
{
	//get Other Component
	AActor* owner = Cast<AActor>(other);
	if (owner) {
		gameData = owner->FindComponentByClass<UGameData>();
	}
	dialogueHandler = p_dialogueHandler;

}

void UChooseTeaHUD::Cancel()
{
	//Hide();
}

void UChooseTeaHUD::Accept()
{
	if (areUSure) {
		dialogueHandler->StartNewDialogue();
	} else if (teaChosen) {

		if (subjectElements.Num() == 0) return;

		selectedSubject = subjectElements[currentIndex]->type;

		subjectElements[currentIndex]->SetSelected(true);
		subjectElements[currentIndex]->SetHover(false);

		SubjectPanel->RenderTransform.Angle = 0.f;
		SubjectPanel->RenderTransform.Scale = FVector2D(1.f);
		SubjectPanel->RenderOpacity = 0.5f;
		SubjectPanel->SynchronizeProperties();

		AreUSurePanel->RenderOpacity = 1.f;
		AreUSurePanel->SynchronizeProperties();

		TeaSelected->SetTea(selectedTea, gameData);
		SubjectSelected->SubjectText->SetText(FText::FromString(gameData->subjectInventory[selectedSubject]));

		areUSure = true;

	}
	else {

		if (teaElements.Num() == 0) return;

		selectedTea = (TeaType)gameData->teaInventory[currentIndex];

		teaElements[currentIndex]->SetHover(false);

		teaElements[currentIndex]->SetSelected(true);

		currentIndex = 0;

		subjectElements[currentIndex]->SetHover(true);

		ShowPanel(false);

		teaChosen = true;
	}

}

void UChooseTeaHUD::MoveX(float value)
{
}

void UChooseTeaHUD::MoveY(float value)
{
	if (teaChosen) {
		MoveYSubject(value);
	}
	else {
		MoveYTea(value);
	}


}

void UChooseTeaHUD::Screen()
{
	AddToPlayerScreen();

	UTeaElementHUD* teaElem;
	if (teaElementHUDClass && gameData) {
		for (size_t i = 0; i < NB_TEATYPE; i++)
		{
			if (gameData->teaInventory[i] != 0) {
				teaElem = CreateWidget<UTeaElementHUD>(GetWorld(), teaElementHUDClass);

				teaElem->SetTea((TeaType)i, gameData);

				TeaElementHolder->AddChild(teaElem);
				teaElements.Add(teaElem);
			}
		}
	}
	currentIndex = 0;
	teaElements[currentIndex]->SetHover(true);

	// Create Subject element
	UGossipElementHUD* subjElem;
	if (subjectElementHUDClass && gameData) {
		for (size_t i = 0; i < gameData->subjectInventory.Num(); i++)
		{
			subjElem = CreateWidget<UGossipElementHUD>(GetWorld(), subjectElementHUDClass);

			subjElem->SubjectText->SetText(FText::FromString(gameData->subjectInventory[i]));

			subjElem->type = i;

			SubjectElementHolder->AddChild(subjElem);
			subjectElements.Add(subjElem);

		}
	}

	ShowPanel(true);
}

void UChooseTeaHUD::Hide()
{
	Reset();

	subjectElements.Empty();

	RemoveFromParent();
}

void UChooseTeaHUD::MoveYTea(float value)
{
	if (value > 0.1 && !movedY) {
		teaElements[currentIndex]->SetHover(false);
		currentIndex--;
		currentIndex = abs(currentIndex % teaElements.Num());
		movedY = true;
		teaElements[currentIndex]->SetHover(true);
	}
	else if (value < -0.1 && !movedY) {
		teaElements[currentIndex]->SetHover(false);
		currentIndex++;
		currentIndex = abs(currentIndex % teaElements.Num());
		movedY = true;
		teaElements[currentIndex]->SetHover(true);
	}
	if (value < 0.1f && value > -0.1f) {
		movedY = false;
	}
}

void UChooseTeaHUD::MoveYSubject(float value)
{
	if (value > 0.1 && !movedY) {
		subjectElements[currentIndex]->SetHover(false);
		currentIndex--;
		currentIndex = abs(currentIndex % subjectElements.Num());
		movedY = true;
		subjectElements[currentIndex]->SetHover(true);
	}
	else if (value < -0.1 && !movedY) {
		subjectElements[currentIndex]->SetHover(false);
		currentIndex++;
		currentIndex = abs(currentIndex % subjectElements.Num());
		movedY = true;
		subjectElements[currentIndex]->SetHover(true);
	}
	if (value < 0.1f && value > -0.1f) {
		movedY = false;
	}

}

void UChooseTeaHUD::Reset()
{
	ShowPanel(true);
	for (UTeaElementHUD* teaElem : teaElements)
	{
		teaElem->RemoveFromParent();
	}
	teaElements.Empty();

	for (UGossipElementHUD* subjElem : subjectElements)
	{
		subjElem->RemoveFromParent();
	}
	subjectElements.Empty();

	currentIndex = 0;

	AreUSurePanel->RenderOpacity = 0.f;
	AreUSurePanel->SynchronizeProperties();

	teaChosen = false;
}

void UChooseTeaHUD::ShowPanel(bool tea)
{
	TeaPanel->RenderTransform.Angle = tea ? -6.f : 0.f;
	TeaPanel->RenderTransform.Scale = FVector2D(tea ? 1.1f : 1.f);
	TeaPanel->RenderOpacity = tea ? 1.f : 0.5f;
	TeaPanel->SynchronizeProperties();

	SubjectPanel->RenderTransform.Angle = tea ? 0.f : 6.f;
	SubjectPanel->RenderTransform.Scale = FVector2D(tea ? 1.f : 1.1f);
	SubjectPanel->RenderOpacity = tea ? 0.5f : 1.f;
	SubjectPanel->SynchronizeProperties();

}

