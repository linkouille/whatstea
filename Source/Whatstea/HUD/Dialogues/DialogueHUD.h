#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"

#include "Whatstea/Dialogues/DialoguesHandler.h"

#include "DialogueHUD.generated.h"

UCLASS()
class WHATSTEA_API UDialogueHUD : public UUserWidget
{
	GENERATED_BODY()
	
public:

	UPROPERTY(EditAnywhere, meta = (BindWidget))
		class UTextBlock* Name;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
		class UTextBlock* DialogueContent;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
		class UImage* NextIcon;


	FString dialogueText;
	UDialoguesHandler* dialoguehandler;

private:
	enum DialogueState
	{
		Start,
		Running,
		Fast,
		Pause,
		End
	};
	DialogueState state;
	float textSpeed = 0.05f;

	int charCount = 0;
	float timeCount = 0;

	bool timeTrigger = false;

	FString textBuffer;

public:
	void StartNewDialogue();

	void SetTextSpeed(float value);

	void SetDialogueContent(FString& text);

	void Update(float deltaTime);

	void UpdateTime(float deltaTime);

	void UpdateState(float deltaTime);

	void Accept();
};
