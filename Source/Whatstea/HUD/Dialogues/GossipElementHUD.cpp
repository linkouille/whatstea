#include "GossipElementHUD.h"
#include "Components/Image.h"
#include "Components/TextBlock.h"
#include "Components/Button.h"
#include "Components/PanelWidget.h"


void UGossipElementHUD::SetSelected(bool state)
{
	selected = state;

}

void UGossipElementHUD::SetHover(bool state)
{
	hover = state;
	SelectedPanel->RenderOpacity = state ? 1.f : 0.f;
	SelectedPanel->SynchronizeProperties();
}

