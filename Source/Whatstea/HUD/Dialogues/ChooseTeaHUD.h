#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Whatstea/GameData.h"
#include "Whatstea/Inputs/InputHandler.h"
#include "Whatstea/Dialogues/DialoguesHandler.h"

#include "ChooseTeaHUD.generated.h"

UCLASS()
class WHATSTEA_API UChooseTeaHUD : public UUserWidget
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, meta = (BindWidget))
		class UButton* OKButton;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
		class UButton* QuitButton;


	UPROPERTY(EditAnywhere, meta = (BindWidget))
		class UPanelWidget* TeaElementHolder;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
		class UPanelWidget* SubjectElementHolder;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
		class UPanelWidget* SubjectPanel;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
		class UPanelWidget* TeaPanel;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
		class UPanelWidget* AreUSurePanel;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
		class UTeaElementHUD* TeaSelected;
	
	UPROPERTY(EditAnywhere, meta = (BindWidget))
		class UGossipElementHUD* SubjectSelected;


	UDialoguesHandler* dialogueHandler;
private:
	TSubclassOf<class UTeaElementHUD> teaElementHUDClass;
	TSubclassOf<class UGossipElementHUD> subjectElementHUDClass;
	
	TArray<class UTeaElementHUD*> teaElements;
	TArray<class UGossipElementHUD*> subjectElements;

	int currentIndex;

	TeaType selectedTea;
	int selectedSubject;

	bool teaChosen = false;
	bool areUSure = false;
	bool movedX = false;
	bool movedY = false;

	UGameData* gameData;


public:
	UChooseTeaHUD(const FObjectInitializer& ObjectInitializer);

	void Setup(UObject* other, UDialoguesHandler* p_dialogueHandler);

	void Cancel();

	void Accept();

	void MoveX(float value);

	void MoveY(float value);

	void Screen();

	void Hide();

private:
	void MoveYTea(float value);

	void MoveYSubject(float value);

	void Reset();

	void ShowPanel(bool tea);

};
