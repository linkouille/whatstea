#include "OptionHUD.h"

#include "Components/Image.h"
#include "Components/TextBlock.h"
#include "Components/PanelWidget.h"
#include "Whatstea/HUD/Interface/UIInteractable.h"
#include "Whatstea/ToolBox.h"
#include "Whatstea/HUD/Option/OptionCategoryHUD.h"
#include "Whatstea/HUD/Elements/CustomComboBox.h"
#include "Whatstea/HUD/Elements/CustomSlider.h"
#include "Whatstea/HUD/Elements/RebindElementHUD.h"


void UOptionHUD::Setup() 
{
	// Category
	categories.Add(ControlsCategory);
	categories.Add(GraphicsCategory);
	categories.Add(SoundCategory);

	categoriesPanel.Add(ControlsPanel);
	categoriesPanel.Add(GraphicsPanel);
	categoriesPanel.Add(SoundPanel);

	// Interactable
	controlsInteractables.Add(CamSlider);

	//HERE REMAPPING

	ResolutionCB->Setup();
	graphicsInteractables.Add(ResolutionCB);
	ScreenModeCB->Setup();
	graphicsInteractables.Add(ScreenModeCB);
	graphicsInteractables.Add(LuminositySlider);
	DaltonienFilterCB->Setup();
	graphicsInteractables.Add(DaltonienFilterCB);

	soundInteractables.Add(GeneralsAudioSlider);
	soundInteractables.Add(MusicAudioSlider);
	soundInteractables.Add(SFXAudioSlider);

	//Build Menu Content
	menuContent.Add(controlsInteractables);
	menuContent.Add(graphicsInteractables);
	menuContent.Add(soundInteractables);

}

void UOptionHUD::SetState(OptionState newState)
{
	categories[(int)state]->SetHover(false);
	categoriesPanel[(int)state]->RenderOpacity = 0.f;
	categoriesPanel[(int)state]->SynchronizeProperties();
	categories[(int)newState]->SetHover(true);
	categoriesPanel[(int)newState]->RenderOpacity = 1.f;
	categoriesPanel[(int)newState]->SynchronizeProperties();
	state = newState;
}

void UOptionHUD::NextState()
{
	if (selectCategory) {

		menuContent[(int)state][currentId]->SetHover(false);
		currentId = (currentId + 1) % menuContent[(int)state].Num();
		menuContent[(int)state][currentId]->SetHover(true);
	}
	else {
		SetState((OptionState)( ((int)state + 1) % categories.Num()) );

	}


}

void UOptionHUD::PrevState()
{
	if (selectCategory) {
		menuContent[(int)state][currentId]->SetHover(false);
		currentId = (currentId - 1) % menuContent[(int)state].Num();
		menuContent[(int)state][currentId]->SetHover(true);
	}
	else {
		SetState((OptionState)(((int)state - 1) % categories.Num()));

	}
}

void UOptionHUD::SelectCategory()
{
	if (selectCategory)
		return;

	selectCategory = true;
	currentId = 0;
	menuContent[(int)state][currentId]->SetHover(true);
}

void UOptionHUD::QuitCategory()
{
	if (!selectCategory)
		return;
	selectCategory = false;
	menuContent[(int)state][currentId]->SetHover(false);
}

void UOptionHUD::Activate(float side)
{
	if (!selectCategory)
		return;

	if (menuContent[(int)state][currentId]->IsComboBox()) {
		UCustomComboBox* comboBox = (UCustomComboBox*)menuContent[(int)state][currentId];
		if (side < 0) comboBox->PrevElement();
		else if (side > 0) comboBox->NextElement();

	} else if (menuContent[(int)state][currentId]->IsSlider()) {
		UCustomSlider* slider = (UCustomSlider*)menuContent[(int)state][currentId];
		if (side < 0) slider->SubToValue();
		else if (side > 0) slider->AddToValue();
	}

	
}

