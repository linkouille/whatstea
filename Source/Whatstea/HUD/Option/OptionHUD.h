#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "OptionHUD.generated.h"

UCLASS()
class WHATSTEA_API UOptionHUD : public UUserWidget
{
	GENERATED_BODY()

public:

	// Category
	UPROPERTY(EditAnywhere, meta = (BindWidget))
		class UPanelWidget* ControlsPanel;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
		class UPanelWidget* GraphicsPanel;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
		class UPanelWidget* SoundPanel;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
		class UOptionCategoryHUD* ControlsCategory;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
		class UOptionCategoryHUD* GraphicsCategory;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
		class UOptionCategoryHUD* SoundCategory;

	// Interactable
	UPROPERTY(EditAnywhere, meta = (BindWidget))
		class UCustomSlider* CamSlider;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
		class UCustomComboBox* ResolutionCB;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
		class UCustomComboBox* ScreenModeCB;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
		class UCustomSlider* LuminositySlider;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
		class UCustomComboBox* DaltonienFilterCB;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
		class UCustomSlider* GeneralsAudioSlider;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
		class UCustomSlider* MusicAudioSlider;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
		class UCustomSlider* SFXAudioSlider;

	enum OptionState
	{
		CONTROLS,GRAPHICS,SOUND, NB_OPTIONSTATE
	};

	OptionState state;
	bool selectCategory = false;

private:
	TArray<class UOptionCategoryHUD*> categories;
	TArray<class UPanelWidget*> categoriesPanel;

	TArray<class IUIInteractable*> controlsInteractables;
	TArray<class IUIInteractable*> graphicsInteractables;
	TArray<class IUIInteractable*> soundInteractables;

	TArray<TArray<class IUIInteractable*>> menuContent;

	int currentId = 0;

public:
	void Setup();

	void SetState(OptionState newState);

	void NextState();

	void PrevState();

	void SelectCategory();

	void QuitCategory();

	void Activate(float side);
};
