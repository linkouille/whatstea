#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/Image.h"
#include "Components/TextBlock.h"
#include "Components/PanelWidget.h"
#include "OptionCategoryHUD.generated.h"

UCLASS()
class WHATSTEA_API UOptionCategoryHUD : public UUserWidget
{
	GENERATED_BODY()

public:


	UPROPERTY(EditAnywhere, meta = (BindWidget))
		class UImage* CategoryBG;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
		class UImage* CategoryBGHover;
private:

	bool hover = false;

public:

	virtual void SetHover(bool state);



};
