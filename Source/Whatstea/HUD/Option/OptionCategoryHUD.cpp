#include "OptionCategoryHUD.h"

void UOptionCategoryHUD::SetHover(bool state)
{
	hover = state;
	CategoryBG->RenderOpacity = state ? 0.f : 1.f;
	CategoryBGHover->RenderOpacity = state ? 1.f : 0.f;

	CategoryBG->SynchronizeProperties();
	CategoryBGHover->SynchronizeProperties();
}
