#include "CustomSlider.h"
#include "Engine/Texture.h"

void UCustomSlider::AddToValue()
{
	value += step;
	value = value > boundValue.Y ? boundValue.Y : value;

	Handle->RenderTransform.Translation.X = (value / boundValue.Y) * 300.f;
	Handle->SynchronizeProperties();
}

void UCustomSlider::SubToValue()
{
	value -= step;
	value = value < boundValue.X ? boundValue.X : value;

	Handle->RenderTransform.Translation.X = (value / boundValue.Y) * 300.f;
	Handle->SynchronizeProperties();
}

float UCustomSlider::GetValue()
{
	return 0.0f;
}

bool UCustomSlider::IsSlider()
{
	return true;
}

void UCustomSlider::SetHover(bool state)
{
	Handle->SetBrushFromTexture(state ? handleTextureHover : handleTexture);
}
