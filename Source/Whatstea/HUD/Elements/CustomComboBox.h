#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/Image.h"
#include "Components/TextBlock.h"
#include "Components/PanelWidget.h"
#include "Whatstea/HUD/Interface/UIInteractable.h"

#include "CustomComboBox.generated.h"

UCLASS()
class WHATSTEA_API UCustomComboBox : public UUserWidget, public IUIInteractable
{
	GENERATED_BODY()
	
public:

	UPROPERTY(EditAnywhere, meta = (BindWidget))
		UTextBlock* CurrentElementText;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
		UImage* ArrowLeft;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
		UImage* ArrowRight;

	UPROPERTY(EditAnywhere)
		TArray<FString> elements;

	UPROPERTY(EditAnywhere)
		class UTexture2D* arrowTexture;

	UPROPERTY(EditAnywhere)
		class UTexture2D* arrowTextureHover;

private:
	int currentElement;

public:

	void Setup();

	void PrevElement();

	void NextElement();

	int GetElement();

	void UpdateText(FString& content);

	virtual bool IsComboBox();

	virtual void SetHover(bool state);
};
