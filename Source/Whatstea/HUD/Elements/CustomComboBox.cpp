#include "CustomComboBox.h"
#include "Engine/Texture.h"


void UCustomComboBox::Setup()
{
	currentElement = 0;

	UpdateText(elements[currentElement]);
}

void UCustomComboBox::PrevElement()
{
	if (currentElement > 0) {
		currentElement--;
		UpdateText(elements[currentElement]);

	}

}

void UCustomComboBox::NextElement()
{
	if (currentElement < elements.Num()-1) {
		currentElement++;
		UpdateText(elements[currentElement]);
	}

}

int UCustomComboBox::GetElement()
{
	return currentElement;
}

void UCustomComboBox::UpdateText(FString& content)
{
	CurrentElementText->SetText(FText::FromString(content));
}

bool UCustomComboBox::IsComboBox()
{
	return true;
}

void UCustomComboBox::SetHover(bool state)
{
	ArrowLeft->SetBrushFromTexture(state ? arrowTextureHover : arrowTexture);
	ArrowRight->SetBrushFromTexture(state ? arrowTextureHover : arrowTexture);
}
