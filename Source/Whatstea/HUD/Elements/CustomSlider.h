#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/Image.h"
#include "Components/TextBlock.h"
#include "Components/PanelWidget.h"
#include "Whatstea/HUD/Interface/UIInteractable.h"

#include "CustomSlider.generated.h"

UCLASS()
class WHATSTEA_API UCustomSlider : public UUserWidget, public IUIInteractable
{
	GENERATED_BODY()
	
public:

	UPROPERTY(EditAnywhere, meta = (BindWidget))
		UImage* Handle;

	UPROPERTY(EditAnywhere)
		class UTexture2D* handleTexture;

	UPROPERTY(EditAnywhere)
		class UTexture2D* handleTextureHover;

	UPROPERTY(EditAnywhere)
		FVector2D boundValue;

	UPROPERTY(EditAnywhere)
		float step;

private:
	float value;

public:
	void AddToValue();

	void SubToValue();

	float GetValue();

	virtual bool IsSlider();

	virtual void SetHover(bool state);

};
