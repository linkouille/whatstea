#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/PanelWidget.h"
#include "ScrapBookHUD.generated.h"

UCLASS()
class WHATSTEA_API UScrapBookHUD : public UUserWidget
{
	GENERATED_BODY()
	
public:

	UPROPERTY(EditAnywhere, meta = (BindWidget))
		class UPanelWidget* ScrapBookPanel;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
		class UPanelWidget* InventoryPanel;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
		class UPanelWidget* GossipPanel;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
		class UPanelWidget* CategoryScrapBook;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
		class UPanelWidget* CategoryInventory;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
		class UPanelWidget* CategorySubject;

	enum ScrapBookState
	{
		Scrapbook,
		Inventory,
		Gossip,
		NB_STATE
	};

private:
	ScrapBookState state;

public:

	void SwitchState(ScrapBookState p_state);

	void CylcleState(int dir);

	UFUNCTION()
	void NextState();

	UFUNCTION()
	void PrevState();

	void SwitchCategoryState(ScrapBookState category, bool p_state);

};
