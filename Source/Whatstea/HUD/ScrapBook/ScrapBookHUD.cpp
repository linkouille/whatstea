#include "ScrapBookHUD.h"
#include "Components/PanelWidget.h"

void UScrapBookHUD::SwitchState(ScrapBookState p_state)
{
	SwitchCategoryState(state, false);
	SwitchCategoryState(p_state, true);

	state = p_state;

}

void UScrapBookHUD::CylcleState(int dir)
{
	ScrapBookState nextState = (ScrapBookState)((state + dir) % NB_STATE);
	SwitchState(nextState);
}

void UScrapBookHUD::NextState()
{
	CylcleState(1);
}

void UScrapBookHUD::PrevState()
{
	CylcleState(-1);
}

void UScrapBookHUD::SwitchCategoryState(ScrapBookState category, bool p_state)
{
	UPanelWidget* categoryPanel;
	UPanelWidget* screenPanel;
	switch (category)
	{
	case UScrapBookHUD::Scrapbook:
		categoryPanel = CategoryScrapBook;
		screenPanel = ScrapBookPanel;
		break;
	case UScrapBookHUD::Inventory:
		categoryPanel = CategoryInventory;
		screenPanel = InventoryPanel;
		break;
	case UScrapBookHUD::Gossip:
		categoryPanel = CategorySubject;
		screenPanel = GossipPanel;
		break;
	default:
		return;
	}

	categoryPanel->RenderTransform.Translation.Y = p_state ? -50.f : 0.f;
	categoryPanel->SynchronizeProperties();

	screenPanel->RenderOpacity = p_state ? 1.f : 0.f;
	screenPanel->SynchronizeProperties();
}
