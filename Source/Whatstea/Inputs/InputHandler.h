#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

#include "EnhancedInputComponent.h"
#include "InputMappingContext.h"
#include "InputAction.h"
#include "EnhancedInputSubsystems.h"

#include "InputHandler.generated.h"

/// <summary>
/// Action Component that handle input bindings (and later remapping) and contain reference to current input mapping
/// </summary>
UCLASS(EditInlineNew, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class WHATSTEA_API UInputHandler : public UActorComponent
{
	GENERATED_BODY()
	
public:

	enum Mapping
	{
		PLAYER,
		UI,
		NB_MAPPING
	};

	enum Actions
	{
		// PlayerMapping
		Pedal_R,
		Pedal_L,
		Turn,
		Brake,
		Cam_X,
		Cam_Y,
		Interact,
		Open_SB,
		//UI Mapping
		AcceptUI,
		MoveUI_X,
		MoveUI_Y,
		CancelUI,
		NextMenu,
		PrevMenu,
		Pause,
		MoveUI_X_Once,
		MoveUI_Y_Once,
		AcceptUI_Released,
		NB_ACTIONS
	};

	UPROPERTY(EditAnywhere, meta = (Don))
		TArray<UInputMappingContext*> mappings;

	UPROPERTY(EditAnywhere)
		TArray<UInputAction*> actions;

private:
	// USed to bind Input Action
	UEnhancedInputComponent* enhancedInputComponent;

	APlayerController* playerControler;

	UInputMappingContext* currentMappings;


public:

	UInputHandler();

	void Setup(UInputComponent* playerInputComponent, APlayerController* pplayerControler);

	void ChangeMapping(Mapping newMapping);

	void BindAction(Actions action, UObject * holder, FName methodName);

	UFUNCTION()
		void DEBUGTEST(const FInputActionValue& value);

private:

	void ImportAsset();

};
