#include "InputHandler.h"

#include "Whatstea/BikeCharacter.h"
#include "GameFramework/PlayerController.h"	
#include "UObject/UObjectGlobals.h"
#include "Misc/Paths.h"

#include "Whatstea/ToolBox.h"

UInputHandler::UInputHandler()
{
	//UE_LOG(LogTemp, Warning, TEXT("I just started running InputHandler"));
	//FString actionsPath[NB_ACTIONS] = {
	//	"/Game/InputData/Actions/Player/Pedal_R.Pedal_R",
	//	"/Game/InputData/Actions/Player/Pedal_L.Pedal_L",
	//	"/Game/InputData/Actions/Player/Turn.Turn",
	//	"/Game/InputData/Actions/Player/Brake.Brake",
	//	"/Game/InputData/Actions/Player/Cam_X.Cam_X",
	//	"/Game/InputData/Actions/Player/Cam_Y.Cam_Y",
	//	"/Game/InputData/Actions/Player/Interact.Interact",
	//	"/Game/InputData/Actions/Player/Open_SB.Open_SB",
	//	//UI Mapping
	//	"/Game/InputData/Actions/UI/AcceptUI.AcceptUI",
	//	"/Game/InputData/Actions/UI/MoveUI_X.MoveUI_X",
	//	"/Game/InputData/Actions/UI/MoveUI_Y.MoveUI_Y",
	//	"/Game/InputData/Actions/UI/CancelUI.CancelUI",
	//	"/Game/InputData/Actions/UI/NextMenu.NextMenu",
	//	"/Game/InputData/Actions/UI/PrevMenu.PrevMenu",
	//	"/Game/InputData/Actions/Player/Pause.Pause",
	//	"/Game/InputData/Actions/UI/MoveUI_X_Once.MoveUI_Once",
	//	"/Game/InputData/Actions/UI/MoveUI_Y_Once.MoveUI_Y_Once",
	//};

	//mappings.Add(LoadObject<UInputMappingContext>(nullptr, TEXT("/Game/InputData/PlayerContext.PlayerContext")));
	//mappings.Add(LoadObject<UInputMappingContext>(nullptr, TEXT("/Game/InputData/UIContext.UIContext")));
	//// Mappings
	///*mappings[0] = LoadObject<UInputMappingContext>(nullptr, TEXT("/Game/InputData/PlayerContext.PlayerContext"));
	//mappings[1] = LoadObject<UInputMappingContext>(nullptr, TEXT("/Game/InputData/UIContext.UIContext"));*/

	//// Actions
	//for (size_t i = 0; i < NB_ACTIONS; i++) {
	//	actions[i] = LoadObject<UInputAction>(nullptr, *actionsPath[i]); 
	//}

}

void UInputHandler::Setup(UInputComponent* playerInputComponent, APlayerController* pplayerControler)
{
	enhancedInputComponent = Cast<UEnhancedInputComponent>(playerInputComponent);

	ImportAsset();

	playerControler = pplayerControler;

	ChangeMapping(PLAYER);

}

void UInputHandler::BindAction(Actions action, UObject* holder, FName methodName)
{
	enhancedInputComponent->BindAction(actions[(int)action], ETriggerEvent::Triggered, holder, methodName);
	//ToolBox::DEBUG_ONSCREEN(methodName.ToString());
}

void UInputHandler::ChangeMapping(Mapping newMapping)
{
	UEnhancedInputLocalPlayerSubsystem* subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(playerControler->GetLocalPlayer());
	
	subsystem->ClearAllMappings();

	subsystem->AddMappingContext(mappings[(int)newMapping], 0);
	currentMappings = mappings[(int)newMapping];
}

void UInputHandler::DEBUGTEST(const FInputActionValue& value) {
	float i = value.Get<float>();
	ToolBox::DEBUG_ONSCREEN_F(i, "Input :");
}

void UInputHandler::ImportAsset()
{
	if (!mappings[0]) ToolBox::DEBUG_ONSCREEN("REEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE");
}
