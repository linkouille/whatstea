// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#define RAY_LEN 100.0f
#define POINT_SIZE 10.0f

/// <summary>
/// Class that contain quality of life function for the dev
/// </summary>
class WHATSTEA_API ToolBox
{
public:
	// In Game Viewport Debug
	/// <summary>
	/// Debug a string in the ingame viewport
	/// </summary>
	/// <param name="content">string to display</param>
	/// <param name="time">Text lifetime (default 0.16f)</param>
	/// <param name="color">Text color (default white)</param>
	void static DEBUG_ONSCREEN(const FString & content, float time=5.0f,FColor color=FColor::White);


	void static DEBUG_ONSCREEN_ERROR(const FString & content);

	/// <summary>
	/// Debug a float in the ingame viewport with the format name:var
	/// </summary>
	/// <param name="var">float to display</param>
	/// <param name="name"></param>
	/// <param name="time">Text lifetime (default 0.16f)</param>
	/// <param name="color">Text color (default white)</param>
	void static DEBUG_ONSCREEN_F(float var, const FString & name, float time = 5.0f, FColor color = FColor::White);

	/// <summary>
	/// Debug a Ray in game viewport
	/// </summary>
	/// <param name="world">Current world (usually AActor::GetWorld())</param>
	/// <param name="start">Starting point of the ray</param>
	/// <param name="dir">Direction of the ray</param>
	/// <param name="color">Ray color (default white)</param>
	void static DEBUG_DRAW_RAY(const UWorld* world, FVector start, FVector dir, FColor color = FColor::White);

	/// <summary>
	/// Debug a point in game viewport
	/// </summary>
	/// <param name="world">Current world (usually AActor::GetWorld())</param>
	/// <param name="pos">Point position</param>
	/// <param name="color">In which color (default white)</param>
	void static DEBUG_DRAW_POINT(const UWorld* world, FVector pos, FColor color = FColor::White);

	void static DEBUG_DRAW_SPHERE(const UWorld* world, FVector pos, float radius, FColor color = FColor::White);

	// Math
	/// <summary>
	/// Clamp a value between a min and a max and return it
	/// </summary>
	/// <typeparam name="T">Operand type (need to have > and < operator implementation)</typeparam>
	/// <param name="a">Value to clamp</param>
	/// <param name="min"></param>
	/// <param name="max"></param>
	/// <returns>Clamped value</returns>
	template<class T> inline static T ClampValue(T a, T min, T max) {
		return a < min ? min : (a > max ? max : a);
	}

	/// <summary>
	/// Return if a is close to b with a precision of s
	/// </summary>
	/// <param name="a"></param>
	/// <param name="b"></param>
	/// <param name="s"></param>
	/// <returns>a > b - s && a < b + s</returns>
	bool inline static NearlyEquals(float a, float b,float s) {
		return a > b - s && a < b + s;
	}
};